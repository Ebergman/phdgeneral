## Will not compile without preamble
I smylink the preamble `note_preamble.tex` around my system to keep
things consistent. You can find it in the folder `extra` but
it may not be the most up to date. Beware, it is not very well organized
and has a lot of unused imports or envs.

Similarly there is a bib file I plan on adding to as time goes on `biblio.bib`.
This can also be found in `extra` and likely to also be out of date.
