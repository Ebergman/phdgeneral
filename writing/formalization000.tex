\documentclass[10pt,numbers=noenddot]{article}

\include{note_preamble}
\begin{document}
\reversemarginpar

\begin{flushright}\texttt{Siamese Algorithm Selection}\end{flushright} \hfill
\lline

\section*{Core Definitions}
\subsection*{Data and Features}

\marginnote{Dataset $\mathcal{D}$}[\parskip]
We define a dataset $\mathcal{D}$ to contain \textbf{instances} $d_1, d_2, \ldots, d_n$
\[
    \mathcal{D} = \set{d_1, d_2, \ldots, d_n}
.\]

If we are only talking about any arbitrary instance, we'll avoid subscripts and use $d$,
if we're talking about a pair we'll use $d_a$ and $d_b$
and if talking about a particular instance in a sequence well use something like $d_i$.
Normally it'll be clear from context.

\marginnote{Feature space $\mathcal{F}$}[\parskip]
Every instance $d$ is really a vector of features from some \textbf{feature space} we'll denote $\mathcal{F}$.
We can formalize this by saying, 
\[
   \mathcal{D} = \set{d_1, d_2, \ldots, d_n \given d _i\in \mathcal{F}}
.\]

\subsection*{Algorithms and Measurements}

\marginnote{Algorithm $\mathcal{A}$}[\parskip]
As we are using several algorithms we will index them from $\mathcal{A}_1, \ldots, \mathcal{A}_k$ where we denote the whole
collection as
\[
    \mathbf{A} = \left(\mathcal{A}_1, \ldots, \mathcal{A}_k\right)
.\]
The same note about data instances and indexing applies as well.

\marginnote{Measurement Space $\mathcal{M}$}[\parskip]
In an attempt to generalize the setting to which this applies, we use $\mathcal{M}$ as a \textbf{measurement space}.
For example, we might want to talk about measuring the error of an algorithm or the success of an algorithm.
This should allow us to talk about both equally.

To reduce introducing new symbols, we'll use some functional notation and let an algorithm $\mathcal{A}$ act as a
function on an instance $d$, going from \textit{feature space} to \textit{measure space}, $\mathcal{A} : \mathcal{F} \to \mathcal{M}$.
 \[
     \mathcal{A}(d) \quad \text{The measure of an algorithm } \mathcal{A} \text{ on an instance } d
.\]

\marginnote{Measure Vector $m$}[\parskip]
It will be useful to collect the measurements of all algorithms in $\mathbf{A}$ on any particular instance $d_i$
as a \textbf{measurement vector}
 \[
     m_i = \left[\mathcal{A}_1(d_i) \, , \, \ldots \, , \, \mathcal{A}_k(d_i)\right], \quad m_i \in \mathcal{M}^k = \mathcal{M} \times \ldots \times \mathcal{M}
.\]
This could represent the error of all the algorithms or equally it could represent the click-through-rate etc..

\clearpage
\section*{Idea Motivation}
Cool, now that we have symbols to represent all the different pieces of what we're given,
we can state a goal of what we'd like to achieve.

\subsection*{The \textit{"best"} algorithm, in hindsight}
We can ask what was the \textit{"best"} algorithm for each instance $d \in \mathcal{D}$ but the definition
of \textit{"best"} is dependant upon what we are measuring. For example, we may wish to choose the
algorithm with the lowest measured error or the one with the highest measured return on investment.
We will sidestep this problem later but for now we can look for minimum or maximum points as our \textit{"best"}.

\marginnote{Best Algorithm $\mathcal{A}_*$}[\parskip]
Suppose for some $d_x \in \mathcal{D}$ we have a measure for each algorithm $\mathcal{A}_1, \mathcal{A}_2, \ldots, \mathcal{A}_k$.
We'll let $\mathcal{A}_*$ denote the \textit{best} algorithm which is either one of $\argmax$ or $\argmin$,
\[
    \mathcal{A}_* = \argmin_{\mathcal{A}_i \in \mathbf{A}} \mathcal{A}_i(d_x)
    \quad \text{or} \quad
    \mathcal{A}_* = \argmax_{\mathcal{A}_i \in \mathbf{A}} \mathcal{A}_i(d_x)
.\]

This can easily be obtained from $m_x$, our vector of measurements for the instance $d_x$ and it is as simple as evaluating
each algorithm on $d_x$.

\subsection*{The \textit{"best"} algorithm, prediction}
The more interesting goal is if we have some instance $d_x \in \mathcal{F}$ and $d_x \notin \mathcal{D}$.
We have no prior information about $d_x$ and prediction of $A_*$ seems like a hopeless task.

One approach is to come up with a \textit{similarity metric} that lets us decide how close $d_x$ is
to all our instances in $\mathcal{D}$ and use that to determine a likely $\mathcal{A}_*$.
There is no single objective way to measure similarity between instances and there has been
work on finding statistical markers that can be of use. We will take a different approach that
relies on directly representing $\mathcal{F}$ in term of algorithm performance.

\section*{Learner, Performance and Embedding}
\marginnote{Performance space $\mathcal{P}$}[\parskip]
We will do this by creating an \textbf{embedding} from $\mathcal{F}$
to some \textbf{performance space} $\mathcal{P}$. This performance space encodes information
about which algorithm $\mathcal{A} \in \mathbf{A}$ is likely to be our best algorithm, $\mathcal{A}_*$.

\marginnote{Learner $\mathcal{L} : \mathcal{F} \to \mathcal{P}$}[\parskip]
As the embedding has to be learnt, we will refer to the algorithm that learns this embedding as a \textbf{learner} $\mathcal{L}$. It's goal
is to learn some representation of $\mathcal{F}$ in vector space $\mathcal{P}$ that allows us to choose the best algorithm for any
instance $d \in \mathcal{F}$.

\marginnote{Performance vector $p$}[\parskip]
When we embed some instance $d \in \mathcal{F}$ into $\mathcal{P}$ we'll refer to its embedded representation as a
\textbf{performance vector} $p$.
\[
    p = \mathcal{L}(d) \in \mathcal{P}
.\]

\newpage
\section*{What to learn? Siamese Training}
To understand our training method, let's look at embedding and how a normal Siamese architecture is trained.

\subsection*{Reasons to Embed}
We will assume that there is some dataset $\mathcal{X} = \set{(x_i, y_i)}$ with instances $x_i$ and their corresponding labels $y_i$
for which we are trying to learn an embedding into a vector space $\sR^n$. This embedding of $x \to v \in \sR^n$ should retain some information that links
an instance $x$ to its label $y$. This could be through proximity ($v$'s close to each other have the same label) or through angle ($v$'s with
similar slope have the same label).

\texttt{\textit{It would be interesting to see what other ways there are of partitioning $\sR^n$ that retain this information beside point distance
and angle. There are other continuous vector spaces such as $\mathbb{C}^n$ that can encode more information but I really wouldn't know how
to go about that.} }

\subsection*{Siamese Training}
\texttt{\textit{Siamese Architecture is a term that confused me for quite a while. The architecture is no different from
a normal embedding (weight sharing just implies its the same network) but it seems to be a particular way of training
an embedding function so I am referring to it as Siamese Training here.}}

We'll refer to the dataset $\mathcal{X} = \set{(a, y_1), (b, y_1), (c, y_2), (d, y_3)}$ as an example. We can see that
$a$ and $b$ share the same label $y_1$ and that $c$ and $d$ have different labels, $y_2$ and $y_3$ respectively (We also assume these labels are categorical and have no
notion of distance between them). We'll also refer to their embedded versions as $v_a, v_b, v_c, v_d \in \sR^n$.

What are some properties that we would like from the embedding?
\begin{itemize}
    \item $v_a$ and $v_b$ should be \textit{"close"}.
    \item $v_d$ and $v_c$ should be \textit{"far"} from each other and $v_a, v_b$.
\end{itemize}

We'll denote the distance between two vectors in the usual euclidean sense of $\norm{v_a - v_b}$ but this could be any
\href{https://en.wikipedia.org/wiki/Metric_space}{metric that respects some properties related to distance}.

Some goals you could give the network to learn the embedding is to minimize the distance between equal labelled points and
maximize the distance between opposing labelled points.
\[
    \max\sum_{(p, y_i)}\left(\sum_{(q, y_j), y_i\neq y_j} \norm{v_p - v_q}\right)
.\]
\[
    \min\sum_{(p, y_i)}\left(\sum_{(q, y_j), y_i=y_j} \norm{v_p - v_q}\right)
.\]
\texttt{\textit{You can think of this as trying to cluster things in the embedded space based on labels and separating out different clusters.
 However I imagine the embedding space must be bounded or some regularization has to happen. Without this, the  \\maximization
goal could be abused, increasing the distance between clusters to\\ arbitrarily large scales.}}

More algorithmically speaking, we can optimize towards these goals by iterating through our dataset $\mathcal{X}$,
selecting an \textbf{anchor} point $(p, y_i)$ and comparing this data point to every other data point $(q, y_j)$ in $\mathcal{X}$.
\begin{itemize}
    \item \textbf{To train towards the minimization goal:} If the points share the same label such that $y_i = y_j$ then we \textit{penalize} the embedding the larger $\norm{v_p - v_q}$ is.
    \item \textbf{To train towards the maximization goal:} If the points have different labels, we \textit{penalize} the embedding for small $\norm{v_p - v_q}$.
\end{itemize}
\texttt{\textit{This is probably quite subject to how the embedding is performed. Neural Nets use the distance as a gradient}} 





\end{document}
