\documentclass[10pt,numbers=noenddot]{article}

\include{note_preamble}
\begin{document}

\begin{flushright}\texttt{Algorithm Selection}\end{flushright} \hfill

\n

\textit{This is an unfinished documents and lacks any outside review.
As such it is subject to change but notation will be kept as
much as possible between versions.}

\textit{The sections can be read in any order but section 1 sets up
most of the core definitions and terms. There are still some
sections to be written, notably clustering in feature and
performance space for a difficulty measure and an idea I
have on a lower bound for performance that any algorithm 
selection method should aim to beat to be useful.}

\textit{To remove the clutter of sub/super scripts where possible,
I will often reintroduce certain objects in each paragraph with 
or without indices,depending on if it's necessary.
Also, if curious, the use of $\id{z}$ instead of something
like $\id{i}$ is because when mixing two indices together,
the common convention of something like $p\id{i}_j$ 
tends confuse me while reading.}

\textit{I probably messed up in some places. If something isn't clear in 
2-3 reads of a paragraph then please let me know!}

\newpage

\section{Algorithms, Points and Spaces}

\marginnote{
    \centering
    \begin{tikzpicture}[auto, scale=0.65]
        \node (F) at (0,0) {\LARGE $\mathcal{F}$};
        \node (S) at (30:5) {\LARGE $\mathcal{P}$};
        \node (P) at (-30:5) {\LARGE $\mathcal{S}$};
        \draw[->, thick] (F) to node[swap] {\large $\mathcal{L}$} (P);
        \draw[->, thick] (F) to node {\large $\bm{A}$} (S);
    \end{tikzpicture}
    \captionof{figure}{Feature, performance and embedding space connected by a set of algorithms $\bm{A}$ and an embedding $\mathcal{L}$.}
}[2cm]

We define a set of algorithms $\bm{A} = \set{\mathcal{A}_1, \mathcal{A}_2, \ldots, \mathcal{A}_k}$
and a set of points $\mathcal{D} = \set{d\id{1}, \ldots, d\id{n}}$ which
represent our data.


The points of $\mathcal{D}$ will have a representation in three space $\mathcal{F}, \mathcal{P}$ and $\mathcal{S}$.

\begin{itemize}[align=left, left=0cm]
    \item [\textbf{Feature Space} $\mathcal{F}$]\n\n
        This is a space of $m$ features from which the points are drawn, $\mathcal{F} = \mathcal{F}_1 \times \ldots \times \mathcal{F}_m$.
        When talking about our points in this space we will use
        \[\color{black}
           \mathcal{D}_{\mathcal{F}} = \set{d\id{1}, \ldots, d\id{n}}
        .\]
    \item [\textbf{Performance Space} $\mathcal{P}$]\n\n
        This is a space such that $\mathcal{P}$ possible performances by our suite
        of algorithms $\bm{A}$, we will formalize this more in a moment. When representing our points in this space
        we will use
        \[\color{black}
            \mathcal{D}_{\mathcal{P}} = \set{p\id{1}, \ldots, p\id{n}}
        .\]
    \item [\textbf{Embedding Space} $\mathcal{S}$]\n\n
        This is a smaller and more \textit{"structured"} space where we will embed $\mathcal{F}$ into,
        using a function from $\mathcal{F} \to \mathcal{S}$. Similarly, when talking about points
        in this space we will use
        \[\color{black}
            \mathcal{D}_{\mathcal{S}} = \set{s\id{1}, \ldots, s\id{n}}
        .\]
\end{itemize}

It should hopefully be clear that each of $\mathcal{D}_{\mathcal{F}}, \mathcal{D}_{\mathcal{P}}, \mathcal{D}_{\mathcal{S}}$ are
the same set of points represented in their respective spaces $\mathcal{F}, \mathcal{P}, \mathcal{S}$.

\subsection{Performances $\mathcal{P}$}

\marginnote{
    \centering
    \begin{tikzpicture}[auto, scale=0.75]
        \node (D) at (2, 4) {\large $\set{d\id{a}, d\id{b}}$};
        \draw[->, thick] (D)+(0, -0.8) to node[right] {$\bm{A}$} (2, 1);
        % bottom half of graph
        \filldraw[gray]
            (3, -2.5) node[above right] {$p\id{a}$} circle [radius=0.1];
        \filldraw[gray]
            (2, -1) node[above right] {$p\id{a}$} circle [radius=0.1];

        \draw[->] (0, -4) -- (4, -4) node[right] {$\mathcal{A}_1$};
        \draw[->] (0, -4) -- (0, 0) node[above] {$\mathcal{A}_2$};
        %\draw[gray, thin] (0, -4) -- (4, 0);
        \node at (2, -4.8) {$\mathcal{P}$};
    \end{tikzpicture}

    \captionof{figure}{$\bm{A}$ takes points  $d\id{a}, d\id{b}$
    to vectors $p\id{a}, p\id{b} \in \mathcal{P}$}
}

As we wish to evaluate the performance of algorithms on point $d\id{z} \in \mathcal{F}$ it will be handy to introduce
functional notation to algorithms, $\mathcal{A}_i: \mathcal{F} \to \mathbb{R}$ for a single algorithm and
$\bm{A} : \mathcal{F} \to \mathcal{P}$ for the suite of algorithms.

\begin{itemize}[align=left, left=0pt]
    \item [\textbf{Single algorithm} $\mathcal{A}_i$ ]\n\n
        Indicating the performance of a single $\mathcal{A}_i$ as a number
         \[\color{black}
             p_i\id{z} = \mathcal{A}_i\left(d\id{z}\right)
        .\]
    \item [\textbf{All algorithms} $\bm{A}$]\n\n
        Indicating the performance of all algorithms $\bm{A}$ as a vector
        \[\color{black}
            p\id{z}
            = \left[p_1\id{z} \, , \ldots, \, p_k\id{z}\right]
            = \bm{A}\left(d\id{z}\right)
        .\]
\end{itemize}

For notational ease, we also define what it means for $\bm{A}$ to act on a set of points $\mathcal{D}_\mathcal{F}$.
        \[\color{black}
            \bm{A}\left(\mathcal{D}_{\mathcal{F}}\right)
            = \bm{A}\left(\set{d\id{1}, \ldots, d\id{n}}\right)
            = \set{p\id{1}, \ldots, p\id{n}}
            = \mathcal{D}_{\mathcal{P}}
        .\]
\newpage
\subsection{Embeddings into $\mathcal{S}$}
The idea for the existence of $\mathcal{S}$ is to represent $\mathcal{F}$ in such a manner that we can more
easily distinguish which $\mathcal{A} \in \bm{A}$ to choose for some point $d \in \mathcal{F}$.

For this to work, we require some way of embedding $\mathcal{F}$ into $\mathcal{S}$. We will call this embedding function a learner $\mathcal{L}$
such that $\mathcal{L} : \mathcal{F} \to \mathcal{S}$. We introduce the symbol $s\id{z}$ to
indicate the representation of $d\id{z} \in \mathcal{F}$ in $S$.
\[\color{black}
    s\id{z} = \mathcal{L}\left(d\id{z}\right)
.\]
We also introduce the shorthand of allowing $\mathcal{L}$ to work on a set of points at a time, similar to how we
defined $\bm{A}$'s action on $\mathcal{D}_{\mathcal{F}}$.
 \[\color{black}
    \mathcal{L}\left(\mathcal{D}_{\mathcal{F}}\right)
    = \mathcal{L}\left(\set{d\id{1}, \ldots, d\id{n}}\right)
    = \set{s\id{1}, \ldots, s\id{n}}
    = \mathcal{D}_{\mathcal{S}}
.\]

\newpage
\section{Objective}
So how do we define our objective?

\marginnote{
    \centering
    \begin{tikzpicture}[auto, scale=0.45]
        \node (rightnode) at (10,0) {|};
        \node (leftnode) at (0,0) {|};
        \node (abest) at (9,0) {};
        \draw[gray] (leftnode) -- (rightnode);
        \draw[black] (abest) + (0,-0.25) -- ++ (0,0.25) node [above] {$\mathcal{A}_*\left(d\right)$};
    \end{tikzpicture}
    \captionof{figure}{Performance of $\mathcal{A}_*$ when we are trying to maximize, a metric such as \textit{"score"}.}
}

Given some point $d \in \mathcal{F}$. We would like to choose an algorithm $\mathcal{A} \in \bm{A}$ that achieves the "best" performance. 
I like to use $*$ for notions of best and optimal so we can notate the "best" algorithm for $d$ as $\mathcal{A}_*$.
\[\color{black}
    \mathcal{A}_* = \argmax_{\mathcal{A} \in \bm{A}} \, \mathcal{A}\left(d\right)
.\]

However, if our performance is instead error we have that we would like to minimize instead,
\[\color{black}
    \mathcal{A}_* = \argmin_{\mathcal{A} \in \bm{A}} \, \mathcal{A}\left(d\right)
.\]
\marginnote{
    \centering
    \begin{tikzpicture}[auto, scale=0.45]
        \node (rightnode) at (10,0) {|};
        \node (leftnode) at (0,0) {|};
        \node (abest) at (1,0) {};
        \draw[gray] (leftnode) -- (rightnode);
        \draw[black] (abest) + (0,-0.25) -- ++ (0,0.25) node [above] {$\mathcal{A}_*\left(d\right)$};
    \end{tikzpicture}
    \captionof{figure}{Performance of $\mathcal{A}_*$ when we are trying to instead minimize, a metric such as \textit{"error"}.}
}

Our goal is then to choose an algorithm $\mathcal{A} \in \bm{A}$ for $d \in \mathcal{F}$
that minimizes the distance from the best possible algorithm $\mathcal{A}_*$ for that point.
This goal can be stated as
\[\color{black}
    \min_{\mathcal{A} \in \bm{A}} \abs{\mathcal{A}\left(d\right) - \mathcal{A}_*\left(d\right)}
.\]

In the ideal case, we have chosen $\mathcal{A} = \mathcal{A}_*$ so that this distance is $0$.

In keeping with our use of the superscript to indicate a certain point, when talking about a point $d\id{z} \in \mathcal{F}$, 
we notate the best algorithm for that point as $\mathcal{A}_*\id{z}$.

\marginnote{
    \centering
    \begin{tikzpicture}[auto, scale=0.45]
        \node (rightnode) at (10,0) {|};
        \node (leftnode) at (0,0) {|};
        \node (abest) at (9,0) {};
        \node (achoice) at (5,0) {};
        \draw[gray] (leftnode) -- (rightnode);
        \draw[black] (abest) + (0,-0.25) -- ++ (0,0.25) node [above] {$\mathcal{A}_*\left(d\right)$};
        \draw[black] (achoice) + (0,-0.25) -- ++ (0,0.25) node [above] {$\mathcal{A}\left(d\right)$};
        \draw[black, <->] (achoice) -- (abest);
    \end{tikzpicture}
    \captionof{figure}{Trying to minimize the distance with our choice $\mathcal{A}\left(d\right)$ to the best algorithm $\mathcal{A}_*\left(d\right)$.}
}

Now suppose we have some way to choose an algorithm $\mathcal{A}\id{z}$ for each $d\id{z} \in \mathcal{D}_\mathcal{F}$.
We can measure the error of our algorithm selection method on $\mathcal{D}_\mathcal{F}$ by
comparing the performance of our algorithm selections from the best possible selection over all
points in $\mathcal{D}_\mathcal{F}$.
\[\color{black}
    \sum_{d\id{z} \in  \mathcal{D}_\mathcal{F}} \abs{\mathcal{A}\id{z}\left(d\id{z}\right) - \mathcal{A}\id{z}_*\left(d\id{z}\right)}
.\]


Equally, in the ideal case, we have chosen $\mathcal{A}\id{z} = \mathcal{A}\id{z}_*$
for each $d\id{z}$ such that the distance in performance is $0$ for all the datapoints.

\newpage
\section{A Baseline, let $\mathcal{S} = \mathcal{P}$}
Given a set of points $\mathcal{D}_{\mathcal{F}}$ and their corresponding performance
$\mathcal{D}_{\mathcal{P}} = \bm{A}\left(\mathcal{D}_{\mathcal{F}}\right)$.
We let $\mathcal{S} = \mathcal{P}$ such that we are embedding directly into performance space.
It should be possible to train a Neural algorithm that learns to approximate the function $\bm{A} : \mathcal{F} \to \mathcal{P}$.
We will refer to this Neural algorithm in particular as $\hat{\mathcal{L}}$ which is the function
\[\color{black}
    \hat{\mathcal{L}}: \mathcal{F} \to \mathcal{P}
.\]
Thus, $\hat{\mathcal{L}}$'s approximation of $\bm{A}$ acts as an embedding of $\mathcal{F}$ into $\mathcal{P}$.

If we have a point $d \in \mathcal{F}$, we notate $\hat{\mathcal{L}}$'s prediction as $\hat{p} = \hat{\mathcal{L}}\left(d\right)$
and when talking about a particular point $d\id{z} \in \mathcal{F}$ we similarly define $\hat{p}\id{z} = \hat{\mathcal{L}\left(d\id{z}\right)}$.

For a point $d \in \mathcal{F}$, we measure the distance between $p = \bm{A}\left(d\right)$ and $\hat{p} = \hat{\mathcal{L}}\left(d\right)$
using the standard euclidean distance $\norm{\hat{p} - p}$. Using this distance, we can train any gradient based learner
to minimize this distance using standard descent methods.
\\ \texttt{\textit{There's room here to experiment with different distance metrics. It might make it easier for  $\hat{\mathcal{L}}$ to
learn $\bm{A}$, it might not\ldots}}

We select the "best" performing algorithm in $\hat{p}\id{z}$ 
by choosing the algorithm corresponding to the index of the min/max in $\hat{p}\id{z}$.
We'll refer to this algorithm as $\hat{\mathcal{A}}_*\id{z}$, the predicted best algorithm for instance $d\id{z}$.
\\ \texttt{\textit{Notating this is prickly due to the min/max nature of "best"\\\ldots Hopefully it should be clear.}}

We can measure $\hat{\mathcal{L}}$'s performance over a set of points $\mathcal{D}_\mathcal{F}$ in two ways.
In each case, the closer the measure is to $0$, the better $\hat{\mathcal{L}}$ is performing.
\begin{itemize}
    \item [Performance difference |]
        This is just the goal statement where our selected algorithm for $d\id{z}$ is $\hat{\mathcal{A}}_*\id{z}$.
        \[\color{black}
            \sum_{d\id{z} \in \mathcal{D}_\mathcal{F}}
            \abs{
                \hat{\mathcal{A}}\id{z}_*\left(d\id{z}\right)
                - \mathcal{A}\id{z}_*\left(d\id{z}\right)
            }
        .\]
        This is the less strict version of the two measurements as we may not be predicting $\hat{p}$ as close
        as possible to $p$ but we only care that the right algorithm was chosen.

    \item [Prediction difference |]
        As we are directly embedding into $\mathcal{P}$, we can use the distance of our prediction to
        the actual performance vector,
        \[\color{black}
            \sum_{d\id{z} \in \mathcal{D}_\mathcal{F}} \norm{\hat{p}\id{z} - p\id{z}}
            =
            \sum_{d\id{z} \in \mathcal{D}_\mathcal{F}} \norm{\hat{\mathcal{L}}\left(d\id{z}\right) - \bm{A}\left(d\id{z}\right)}
        .\]
        This is the more strict version of the two measurements as we are measuring $\hat{\mathcal{L}}$'s ability
        to predict the correct performance vector, not just that it selects the correct algorithm.
\end{itemize}

\newpage
\section{Siamese Architectures for Embedding}
A Siamese architecture seems to be often explained through the shared weights architecture, two
learners $\mathcal{L}_1$ and $\mathcal{L}_2$ sharing the same weights and trained using a combination of the distance between
their outputs and prior some prior knowledge about what these distances should be.
I find it more informative to think of it as a method of training a single learner $\hat{\mathcal{L}}$.
If this is the case then Siamese Architectures should really just be considered as a way to train
an embedding rather than a real architectural difference, illustrating the point that what really 
matters is not the architecture but where and what points you choose as well as how you choose their distances.

If this section is to be believed, I think talking generally about embeddings and training is more beneficial
than the use of the term Siamese Architecture.

I'll try show their equivalence now.

To do so I will show that $\mathcal{L}_1, \mathcal{L}_2$ and $\hat{\mathcal{L}}$ will produce the same output set
given the same input set. I will then show that data for training
$\mathcal{L}_1$ and $\mathcal{L}_2$ can equally be used to train a single $\hat{\mathcal{L}}$.
I will try to do this without introducing any symbol for weights and do so using only their
input set $\mathcal{D}_\mathcal{F}$ and their embedded output $\mathcal{D}_\mathcal{S}$.

\subsection{Asserting $\mathcal{L}_1\left(\mathcal{D}_\mathcal{F}\right) = \mathcal{L}_2\left(\mathcal{D}_\mathcal{F}\right)
= \hat{\mathcal{L}}\left(\mathcal{D}_\mathcal{F}\right)$}
Let's first assume we have a batch of data $\mathcal{D}_\mathcal{F}$. Passing this through the functions $\mathcal{L}_1$ and
$\mathcal{L}_2$ we get their respective embedding of the points, $\mathcal{D}_{\mathcal{S}_1}$ and $\mathcal{D}_{\mathcal{S}_2}$.

If the weights of $\mathcal{L}_1$ and $\mathcal{L}_2$ are tied then they will perform the same function
on any $d \in \mathcal{D}_\mathcal{F}$. As such we have,
\begin{align*}
    & \mathcal{L}_1\left(d\right) = \mathcal{L}_2\left(d\right)
    \\
    \implies & \mathcal{L}_1\left(\set{d\id{1}, \ldots, d\id{n}}\right) = \mathcal{L}_2\left(\set{d\id{1}, \ldots, d\id{n}}\right),
    \quad \forall d\id{z} \in \mathcal{D}_\mathcal{F}
    \\
    \implies & \mathcal{L}_1\left(\mathcal{D}_\mathcal{F}\right) = \mathcal{L}_2\left(\mathcal{D}_\mathcal{F}\right)
    \\
    \implies & \mathcal{D}_{\mathcal{S}_1} = \mathcal{D}_{\mathcal{S}_2},
\end{align*}
that is they both produce the same embedding for each datapoint, implying that we get the same
set out embedded points.
\texttt{\textit{This should be intuitive from the shared weights but this is just a more concrete argument.}}

As we have stated that $\hat{\mathcal{L}}$ has the same weights as both $\mathcal{L}_1$ and $\mathcal{L}_2$
then by substituting $\hat{\mathcal{L}}$ for either learner, we arrive at our claim
$\mathcal{L}_1\left(\mathcal{D}_\mathcal{F}\right)
 = \mathcal{L}_2\left(\mathcal{D}_\mathcal{F}\right)
 = \hat{\mathcal{L}}\left(\mathcal{D}_\mathcal{F}\right)$.

As each of them produce the same set of embedded points, we will simply refer to this embedded set of points 
as $\mathcal{D}_\mathcal{S}$.

\subsection{Training $\mathcal{L}_1$ and $\mathcal{L}_2$ can be made equivalent to training $\hat{\mathcal{L}}$}
Suppose that we have that $\mathcal{L}_1 = \mathcal{L}_2 = \hat{\mathcal{L}}$ by enforcing that they
each share the same weights.

If we can produce a training pair of embedded points
$\left(s_a,s_b\right) = \big(\mathcal{L}_1\left(d_a\right), \mathcal{L}_2\left(d_b\right)\big)$
using our Siamese Architecture $\mathcal{L}_1, \mathcal{L}_2$,
then we can also produce that same pair with our single learner, \textit{i.e.}
\[\color{black}
    \left(s_a,s_b\right)
    = \big(\mathcal{L}_1\left(d_a\right), \mathcal{L}_2\left(d_b\right)\big)
= \big(\hat{\mathcal{L}}\left(d_a\right), \hat{\mathcal{L}}\left(d_b\right)\big)
.\]
As any update to the weights are tied and all possible pairs $\left(s_a,s_b\right)$
can be produced by both the Siamese architecture $\mathcal{L}_1, \mathcal{L}_2$
and the single learner $\hat{\mathcal{L}}$, we have that any training
procedure for $\mathcal{L}_1, \mathcal{L}_2$ can be described for a single learner $\hat{\mathcal{L}}$.

\texttt{\textit{I haven't really proved training procedures are equivalent but
rather that $\hat{\mathcal{L}}$ can be trained to produce the same output and that
is has access to all the same \\information that can be used for training}}.

\newpage
\section{Normalizing instances in $\mathcal{P}$ using numeric labels}
We will first go through some definition setup to do with labelled datasets, linking
it to the general notation and introduce some assumptions required.
We will then go on and talk about a normalization function $f_M: \mathcal{P} \times \mathcal{F}_\mathbb{R} \to \mathcal{P}$,
a function that can take a set of points $\mathcal{D}_\mathcal{P}$ to a set $\mathcal{D}'_\mathcal{P}$
which has our data normalized based on numeric labels and a bound $B$.

\texttt{\textit{This section is not complete and the function $f_B$ does
not exist as intended at the moment. It is the best I could do in short notice.}}

\subsection{A concrete performance evaluation and extending the notation to include labels}
To introduce the notion of labels to a datapoint we extend $\mathcal{F}$
by a single dimension to obtain,
\[\color{black}
    \mathcal{F} = \mathcal{F}_1 \times \ldots \times \mathcal{F}_m \times \mathcal{F}_\mathbb{R}
.\]
Here $\mathcal{F}_\mathbb{R}$ is indicating our numeric labels.

We also introduce the fact that $d \in \mathcal{F}$ is made up of features
$\left(d_1, \ldots, d_m, y\right)$ where $y \in \mathcal{F}_\mathbb{R}$ is the numeric label.
We will not need to talk about the features $d_1, d_2, \ldots, d_m$.
We will also assume that any $y \in \mathcal{F}_\mathbb{R}$ is bounded by $0 \leq y \leq B$.
\\ \texttt{\textit{The use of $\mathbb{R}$ in $\mathcal{F}_\mathbb{R}$ is used to 
        highlight that it is a numeric set and
        distinct from the indexing of the other features.}}


Including indices into the mix we have that
\[\color{black}
    d\id{z} = \left(d\id{z}_1, \ldots, d\id{z}_m, y\id{z}\right)
,\]
with $y\id{z}$ being the label for the point $d\id{z}$.

We can use $\hat{y}\id{z}_i$ to indicate the predicted
label of $\mathcal{A}_i$ on and instance $d\id{z}$.

As we have already used the functional notation of $\mathcal{A}$ to
indicate a function from feature space $\mathcal{F}$ to performance
space $P$, we \textbf{cannot} use it to indicate the label predicted
of $\mathcal{A}$ on an instance $d \in \mathcal{F}$ \textit{i.e.}
we cannot define $\hat{y}\id{z}_i = \mathcal{A}_i\left(d\id{z}\right)$
as this is reserved for its performance $p\id{z}_i$.

There are two work arounds I can think of,
\begin{itemize}
    \item [Just define it into existance |]
        This is by far the easiest choice and we could simply just say that for any
        $d\id{z} \in \mathcal{F}$ and $\mathcal{A}_i \in \bm{A}$, we can indicate its predicted label as $\hat{y}\id{z}_i$,
        ignoring how we get there.
    \item [$L : \bm{A} \times \mathcal{F} \to \mathcal{F}_\mathbb{R}$ |]
        Another alternative is wrap the label predicting functionality of $\mathcal{A}_i$ by having
        some function $L\left(\mathcal{A}_i, d\id{z}\right) = \hat{y}\id{z}_i$.
\end{itemize}

It doesn't really matter for this but if you are to talk about the process of prediction for algorithms quite
a bit, it may be handy to use some function $L\left(\cdot,\cdot\right)$. We will go
with option 1 and simply just define it into existence.

In this example we are computing the \textbf{absolute error} of an algorithm $\mathcal{A}_i$ on a point
$d\id{z}$,
\[\color{black}
    p\id{z}_i = \mathcal{A}_i\left(d\id{z}\right) = \vert \hat{y}\id{z}_i - y\id{z} \vert
.\]

The vector $p\id{z} = \bm{A}\left(d\id{z}\right)$ is still defined as in section 1.
\\ \texttt{\textit{I've never really written LaTeX documents long enough to have to self reference equations and such. I'll
fix this up in a later version, rather than simply saying "section 1".}}

\subsection{A normalizing function $f : \mathcal{P} \times \mathcal{F}_\mathbb{R} \to \mathcal{P}$}
From my understanding, the use of this normalizing function is to that training some learner $\mathcal{L}$, we would
like performances deemed to be \textit{"similar"} to be \textit{"close"} by use of the euclidean or cosine metric. For example,
consider three performance vectors,
\begin{align*}
    p\id{1} &= \left[ 9, 1\right] \\
    p\id{2} &= \left[ 9.5, 0.5\right] \\
    p\id{3} &= \left[ 0.9, 0.1\right]
\end{align*}
Here we would consider each of these three points as \textit{"similar"} as all three have $\mathcal{A}_1$
as the best performing algorithm,
\[\color{black}
    \mathcal{A}_*\id{1} = \mathcal{A}_*\id{2} = \mathcal{A}_*\id{3} = \mathcal{A}_1
.\]
Looking at our points, $p\id{1}$ is close to $p\id{2}$ by the euclidean metric and $p\id{1}$
and $p\id{3}$ have a distance of $0$ under the cosine metric.

To normalize these points such that this \textit{"similarity"} of the best performing algorithm $\mathcal{A}_*$
is captured, we propose the following normalization function $f_B : \mathcal{P} \times \mathcal{F}_\mathbb{R} \to \mathcal{P}$,
that is, a function parameterized by a bound $B$ that acts on a performance vector and a label to
produce a new point in performance space.
\begin{align*}
    f_B\left(p\id{z}, y\id{z}\right) =
\end{align*}

\texttt{\textit{For singular instances as listed in the slides, we
can just use a function $h_B: \mathbb{R} \times \mathcal{F}_R$ that
works on a single performance at a time.}}
\begin{align*}
    f_B\left(p\id{z}, y\id{z}\right) &= \big[h_B\left(p\id{z}_1, y\id{z}\right), \ldots, h_B\left(p\id{z}_k, y\id{z}\right)\big]
    \\
    \\
    h_B\left(p\id{z}_i, y\id{z}\right) &= \left(1 - \frac{p\id{z}_i}{\epsilon\left(y\id{z}\right)}\right) \cdot \frac{\norm{p\id{z}}_{<}}{p\id{z}_i}
    \\
    \\
                            &= \norm{p\id{z}}_{<}\left(\frac{1}{p\id{z}_i} - \frac{1}{\epsilon\left(y\id{z}\right)}\right)
\end{align*}

We used $\epsilon_B\left(y\id{z}\right)$ to indicate the maximum possible error based on a label $y\id{z}$.
This is necessarily parameterized by our max bound $B$.
\[\color{black}
    \epsilon_B\left(y\id{z}\right) = \max\left(B - y\id{z}, y\id{z} - 0\right)
.\]
We also use $\norm{\cdot}_{<}$ to indicate the minimum of a vector.

\texttt{\textit{The equation as listed in the slides can't be converted to vector
    notation as it would require $\frac{1}{p\id{z}}$ which is not a valid operation
    on vectors due to the possibility of a divide by $0$ error, arising from an
    element of $p\id{z}$ being $0$. This highlights the fact the "relative intra performance"
    also could result in a division by $0$. This happens in the case where an algorithm
    would have no error. While in practice, this could probably be avoided by substituting
    a very small number, for efficient computational purposes I would guess that
    this would be a major bottleneck as it can't be safely done on a GPU. (No error checking)}}

\texttt{\textit{I'm used to the max of a vector being nicely expressed using $\norm{\cdot}_\infty$
        but I'm unaware of a compact notation for min of a vector, hence the use of $\norm{\cdot}_{<}$
        which hopefully expresses the notions of "size" and "smallest".}}

\texttt{\textit{If we consider more custom distance metrics than euclidean and cosine, 
        the performance points do not need to be moved about, we simply
        change how we measure the distance between points to capture the notions of "similarity".}}


\end{document}
