\documentclass[10pt,numbers=noenddot]{article}

\include{note_preamble}
\begin{document}
\reversemarginpar

\begin{flushright}\texttt{Siamese Algorithm Selection}\end{flushright} \hfill

\lline

\textbf{To reader}: \texttt{In general, if I define something with a sub/super script like $p^{\left(z\right)}$ or $\mathcal{A}_i$,
        the sub/super script is only defined for that paragraph or section, similar to scoping in programming.
        If something seems ambiguous or unclear after 2-3 reads of that paragraph, I've probably messed up.
        \\  I also tend to replace certain keywords with a defined symbol, for example, if we define $\mathcal{A}$
        to be an "algorithm" and $d$ as a "datapoint" then I might write something like, ".. find an $\mathcal{A}$ that will perform well for some $d$".
        \\ Feel free to ask for clarification or throw an angry face my way if something isn't clear.}

\section{Problem Setup}
We are given a set of algorithms $\bm{A} = \set{\mathcal{A}_1, \mathcal{A}_2, \ldots, \mathcal{A}_m}$
and some data point $d$ in a feature space $\mathcal{F}$. What is the \textit{"best"} algorithm for
that datapoint?

Essentially we would like to know something about the structure of $\mathcal{F}$ that will
gives us some clue to as to which $\mathcal{A}$ to pick. The problem here is that $\mathcal{F}$ can be huge,
unstructured and noisy when trying to relate it to performances. Attempting to cluster in $\mathcal{F}$ seems
a hopeless task as the number of features increases.

To simplify our problem, we will create an embedding from $\mathcal{F} \to \mathcal{S}$, a
smaller and more compact space whose structure encodes information about the performance of all algorithms in $\bm{A}$
for every data point in $\mathcal{F}$.

Now suppose we are given some data point $d \in  \mathcal{F}$. To chose an algorithm, all
we are required to do is use our embedding to represent $d$ in $\mathcal{S}$ where the choice
of which $\mathcal{A}$ to choose is \textit{"obvious"}.

There are some clear questions that arise.
\begin{itemize}
    \item What is our definition of the \textit{"best"} $\mathcal{A} \in \bm{A}$ for some $d \in \mathcal{F}$?
    \item What structure on $\mathcal{S}$ would make choosing $\mathcal{A}$ obvious?
    \item How do we create an embedding from $\mathcal{F} \to \mathcal{S}$ where this structure exists?
\end{itemize}
We will approach each of these questions in turn.

\section{Some preliminary definitions on Data and Performance}
In the real world, we are often given a \textbf{dataset} $\mathcal{D} = \set{d\id{1}, d\id{2}, \ldots, d\id{n}}$. It's helpful to
think of $\mathcal{D}$ as a subset of points from $\mathcal{F}$, a space consisting of $k$ different features.
\[\color{black}
    \mathcal{D} = \set{d\id{1}, d\id{2}, \ldots, d\id{n}} \quad \quad
    \mathcal{D} \subset \mathcal{F} = \mathcal{F}_1 \times \ldots \times \mathcal{F}_k
.\]
To nail in the definitions, we will treat each $\mathcal{A} \in \bm{A}$ as a function $\mathcal{A} : \mathcal{F} \to \sR$,
going from $\mathcal{F}$ to some number $p$ representing its performance on some $d \in \mathcal{F}$.

As we are going to be talking about a particular \textbf{performance} of a specific algorithm $\mathcal{A}_i$ on a specific instance $d\id{z}$,
we define $p_i^{\left(z\right)}$ as
\[\color{black}
    p_i^{\left(z\right)} = \mathcal{A}_i\left(d\id{z}\right)
.\]
\texttt{\textit{I use $z$ as an index over something like $j$, small script $_i$, $_j$ look quite little similar when put too close together.}}

It will also be helpful to talk about the performance of all $\mathcal{A}_1, \mathcal{A}_2, \ldots, \mathcal{A}_m$ on an instance $d\id{z} \in \mathcal{F}$. We can
do this by collecting all the individual performances $p_1^{\left(z\right)}, p_2^{\left(z\right)}, \ldots, p_m^{\left(z\right)}$ into a \textbf{performance vector} $p^{\left(z\right)}$.
\[\color{black}
    p^{\left(z\right)} = \left[ p_1^{\left(z\right)} \, , \, \ldots \, , \, p_m^{\left(z\right)} \right]
.\]
Since we're talking about spaces, we introduce the symbol $\mathcal{P}$ to indicate the \textbf{performance space}, representing the space of performance vectors
that are possible. 

In general we may expect that $\mathcal{P} \subset \sR^m$ instead of $\mathcal{P} = \sR^m$ as we often our performances have some upper and lower bound.
\\ \textit{\texttt{I only mention this point as I have some idea of converting $\mathcal{P}$ to a special subspace of $\sR^m$ called an
        \href{https://en.wikipedia.org/wiki/Simplex}{m-simplex} which is a well understood and studied space.} }

\section{The best algorithm}
We can now safely define the "best" algorithm in the well understood sense. Given a point $d\id{z} \in \mathcal{F}$,
we denote $\mathcal{A}_*^{\left(z\right)}$ as the best performing algorithm for that instance and $p\id{z}_*$ as the corresponding best
performance.
\begin{align*}
    \mathcal{A}\id{z}_* &= \argmax_{\mathcal{A}_i \in \bm{A}} \quad \mathcal{A}_i\left(d\id{z}\right)
    \\
    p\id{z}_* &= \argmax_{\mathcal{A}_i \in \bm{A}} \quad p\id{z}_i
\end{align*}

\textit{\texttt{If our performance is measuring error instead, we can replace $\argmax$ with $\argmin$ or
invert error into performance, if possible.} }

\textbf{Interesting Side note:}

For some $\mathcal{D} = \set{d\id{1}, d\id{2}, \ldots, d\id{n}}$, in our case we would ideally like to be able
to select the corresponding $\mathcal{A}_*\id{1},\mathcal{A}_*\id{2}, \ldots, \mathcal{A}_*\id{n}$.
In my understanding, \textbf{Online Regret Minimization} would treat each algorithm as an "expert" and focuses on the question
of being as close as possible to the overall best performing algorithm $\mathcal{A}^*$.
\[\color{black}
    \mathcal{A}^* = \argmax_{\mathcal{A} \in \bm{A}} \sum_{z=1}^n \mathcal{A}\left(d\id{z}\right)
.\]

However, this setting often assumes that we can't even look at $d\id{z}$ before we have to choose an $\mathcal{A}$.
In our setting we should be able to do much better, looking at $d\id{z}$ before choosing. This gives a lower bound on performance
we should be able to outperform. If we are not outperforming an algorithm that requires no knowledge of $d$ or $\mathcal{F}$
then clearly there is an issue with the idea. Using this lower bound and the upper bound of choosing each algorithm correctly for every instance,
we can have a sense of how much information we are effectively utilizing from $\mathcal{F}$.


\section{Now the hard part, $\mathcal{F} \to \mathcal{S}$}
There are many inventive ways to try and create an embedding but we will go with a \textbf{Siamese N.N.} that
is trained to extract and represent useful features from $\mathcal{F}$ to $\mathcal{S}$. \\\textit{---Omitted explanation
of Siamese Neural Net---}.

We'll refer to our Siamese N.N. as a \textit{learner} or more simply as $\mathcal{L}$. That is, our learner $\mathcal{L}$
learns a function
 \[\color{black}
     \mathcal{L} : \mathcal{F} \to \mathcal{S}
 .\]
The issue is that $\mathcal{L}$ requires \textit{labelled} training pairs which we are assuming are not part of $\mathcal{F}$.
This is where I believe the crux of the idea lies, \textbf{using algorithm performance as a pseudo label.}

\end{document}
