\documentclass[10pt,numbers=noenddot]{article}

\include{note_preamble}
\begin{document}
\reversemarginpar

\begin{flushright}\texttt{Algorithm Selection}\end{flushright} \hfill

\lline

\texttt{This is an unfinished documents and lacks any outside review. As such it is subject to change
but notation will be kept as much as possible between versions.}

\texttt{The sections can be read in any order but section 1 sets up most of the core definitions and terms.
There are still some sections to be written, notably clustering in feature and performance space for
a difficulty measure and an idea I have on a lower bound for performance that any algorithm selection
method should aim to beat to be useful.}

\texttt{To remove the clutter of sub/super scripts where possible, I will often reintroduce
certain objects in each paragraph with or without indices,depending on if it's necessary.
Also, if curious, the use of $\id{z}$ instead of something like $\id{i}$ is because when mixing two indices together,
the common convention of something like $p\id{i}_j$ tends confuse me while reading.}

\texttt{I probably messed up in some places. If something isn't clear in 2-3 reads of a paragraph then please let me know!}
\newpage

\section{Algorithms, Data and Spaces}
We define a set of algorithms $\bm{A} = \set{\mathcal{A}_1, \mathcal{A}_2, \ldots, \mathcal{A}_k}$
and a dataset of points $\mathcal{D} = \set{d\id{1}, \ldots, d\id{n}}$.

A dataset has representations in three different spaces.
\begin{itemize}
    \item [Feature Space $\mathcal{F}$ |]
        This is a space of $m$ features from which the instances are drawn, $\mathcal{F} = \mathcal{F}_1 \times \ldots \times \mathcal{F}_m$.
        When talking about our data $\mathcal{D}$ in this space we will use
        $\mathcal{D}_{\mathcal{F}} = \set{d\id{1}, \ldots, d\id{n}}$.

    \item [Performance Space $\mathcal{P}$ |]
        This is a space such that $\mathcal{P} \subset \mathbb{R}^k$, representing all possible performances by our suite
        of algorithms $\bm{A}$, we will formalize this more in a moment. When representing our dataset in this space
        we will use $\mathcal{D}_{\mathcal{P}} = \set{p\id{1}, \ldots, p\id{n}}$.

    \item [Embedding Space $\mathcal{S}$ |]
        This is a smaller and more \textit{"structured"} space where we will attempt to embed $\mathcal{F}$ into,
        using a function from $\mathcal{F} \to \mathcal{S}$. Similarly, when talking about points
        in this space we will use the notation $\mathcal{D}_{\mathcal{S}} = \set{s\id{1}, \ldots, s\id{n}}$.
\end{itemize}

It should hopefully be clear that each of $\mathcal{D}_{\mathcal{F}}, \mathcal{D}_{\mathcal{P}}, \mathcal{D}_{\mathcal{S}}$ are
the same set of points represented in their respective spaces $\mathcal{F}, \mathcal{P}, \mathcal{S}$.

\subsection{Performances $\mathcal{P}$}
As we wish to evaluate the performance of algorithms on points in a set $\mathcal{D}_\mathcal{F}$ it will be handy to introduce
functional notation to algorithms.
\begin{itemize}
    \item [Performance of $\mathcal{A}_i \in \bm{A}$ |]
        When talking about the performance of an $\mathcal{A}_i \in \bm{A}$ on some point $d\id{z} \in \mathcal{F}$ we will
        do so by letting $\mathcal{A}_i : \mathcal{F} \to \mathbb{R}$, a function from feature space to some real measurement.
        It will also be handy to introduce the symbol $p_i\id{z}$ to indicate $\mathcal{A}_i$'s performance on $d\id{z}$,
        defined by
         \[\color{black}
             p_i\id{z} = \mathcal{A}_i\left(d\id{z}\right)
        .\]
    \item [Performance of $\bm{A}$ |]
        As we need to evaluate all algorithms on a point $d\id{z} \in \mathcal{F}$, it will be handy to be able
        to talk about $\bm{A}$ as a set but also as a function $\bm{A} : \mathcal{F} \to \mathcal{P}$. This
        ties back into our previous notation for $\mathcal{D}_{\mathcal{P}} = \set{p\id{1}, \ldots, p\id{k}}$
        where we define $p\id{z}$ to be
        \[\color{black}
            p\id{z}
            = \bm{A}\left(d\id{z}\right)
            = \left[\mathcal{A}_1\left(d\id{z}\right), \ldots, \mathcal{A}_k\left(d\id{z}\right)\right]
            = \left[p_1\id{z} \, , \ldots, \, p_k\id{z}\right]
        .\]
        This should hopefully be understood as a vector representing the performance of all algorithms on a point $d\id{z}$.
        For notational ease, we also define
        \[\color{black}
            \bm{A}\left(\mathcal{D}_{\mathcal{F}}\right)
            = \bm{A}\left(\set{d\id{1}, \ldots, d\id{n}}\right)
            = \set{p\id{1}, \ldots, p\id{n}}
            = \mathcal{D}_{\mathcal{P}}
        .\]
        Hence, $\bm{A}$ can act as a function taking a set of points in $\mathcal{F}$ to $\mathcal{P}$.
\end{itemize}

\subsection{Embeddings into $\mathcal{S}$}
The idea for the existence of $\mathcal{S}$ is to represent $\mathcal{F}$ in such a manner that we can more
easily distinguish which $\mathcal{A} \in \bm{A}$ to choose for some point $d \in \mathcal{F}$.

For this to work, we require some way of embedding $\mathcal{F}$ into $\mathcal{S}$. We will call this embedding function a learner $\mathcal{L}$
such that $\mathcal{L} : \mathcal{F} \to \mathcal{S}$. We introduce the symbol $s\id{z}$ to
indicate the representation of $d\id{z} \in \mathcal{F}$ in $S$.
\[\color{black}
    s\id{z} = \mathcal{L}\left(d\id{z}\right)
.\]
We also introduce the shorthand of allowing $\mathcal{L}$ to work on a set of points at a time, similar to how we
defined $\bm{A}$'s action on $\mathcal{D}_{\mathcal{F}}$.
 \[\color{black}
    \mathcal{L}\left(\mathcal{D}_{\mathcal{F}}\right)
    = \mathcal{L}\left(\set{d\id{1}, \ldots, d\id{n}}\right)
    = \set{s\id{1}, \ldots, s\id{n}}
    = \mathcal{D}_{\mathcal{S}}
.\]

\newpage
\section{Goal Statements}
So how do we define our objective?

Given some point $d \in \mathcal{F}$. We would like to choose an $\mathcal{A} \in \bm{A}$ that achieves the "best" performance. 
I like to use $*$ for notions of best and optimal so we can notate the "best" algorithm for $d$ as $\mathcal{A}_*$.
\[\color{black}
    \mathcal{A}_* = \argmax_{\mathcal{A} \in \bm{A}} \, \mathcal{A}\left(d\right)
.\]

However, if our performance is instead error we have that we would like to minimize instead,
\[\color{black}
    \mathcal{A}_* = \argmin_{\mathcal{A} \in \bm{A}} \, \mathcal{A}\left(d\right)
.\]
To keep things consistent, when talking about specific points $d\id{z} \in \mathcal{F}$, we notate the best algorithm
for that point as $\mathcal{A}_*\id{z}$.

Our goal is then to choose an algorithm $\mathcal{A} \in \bm{A}$ for $d$ that minimizes the distance from the best possible
algorithm $\mathcal{A}_*$.
This goal can be stated as
\[\color{black}
    \argmin_{\mathcal{A} \in \bm{A}} \abs{\mathcal{A}\left(d\right) - \mathcal{A}_*\left(d\right)}
.\]
In the ideal case, we have chosen $\mathcal{A} = \mathcal{A}_*$ so that this distance is $0$.

Now suppose we have some way to choose an algorithm $\mathcal{A}\id{z}$ for each $d\id{z} \in \mathcal{D}_\mathcal{F}$.
We can measure the error of our algorithm selection method on $\mathcal{D}_\mathcal{F}$ by
comparing the performance of our algorithm selections from the best possible selection over all
points in $\mathcal{D}_\mathcal{F}$.
\[\color{black}
    \sum_{d\id{z} \in  \mathcal{D}_\mathcal{F}} \abs{\mathcal{A}\id{z}\left(d\id{z}\right) - \mathcal{A}\id{z}_*\left(d\id{z}\right)}
.\]
Equally, in the ideal case, we have chosen $\mathcal{A}\id{z} = \mathcal{A}\id{z}_*$
for each $d\id{z}$ such that the distance in performance is $0$ for all the datapoints.

\newpage
\section{A Simple Idea, let $\mathcal{S} = \mathcal{P}$}
Given a set of points $\mathcal{D}_{\mathcal{F}}$ and their corresponding performance
$\mathcal{D}_{\mathcal{P}} = \bm{A}\left(\mathcal{D}_{\mathcal{F}}\right)$.
We let $\mathcal{S} = \mathcal{P}$ such that we are embedding directly into performance space.
It should be possible to train a Neural algorithm that learns to approximate the function $\bm{A} : \mathcal{F} \to \mathcal{P}$.
We will refer to this Neural algorithm in particular as $\hat{\mathcal{L}}$ which is the function
\[\color{black}
    \hat{\mathcal{L}}: \mathcal{F} \to \mathcal{P}
.\]
Thus, $\hat{\mathcal{L}}$'s approximation of $\bm{A}$ acts as an embedding of $\mathcal{F}$ into $\mathcal{P}$.

If we have a point $d \in \mathcal{F}$, we notate $\hat{\mathcal{L}}$'s prediction as $\hat{p} = \hat{\mathcal{L}}\left(d\right)$
and when talking about a particular point $d\id{z} \in \mathcal{F}$ we similarly define $\hat{p}\id{z} = \hat{\mathcal{L}\left(d\id{z}\right)}$.

For a point $d \in \mathcal{F}$, we measure the distance between $p = \bm{A}\left(d\right)$ and $\hat{p} = \hat{\mathcal{L}}\left(d\right)$
using the standard euclidean distance $\norm{\hat{p} - p}$. Using this distance, we can train any gradient based learner
to minimize this distance using standard descent methods.
\\ \texttt{\textit{There's room here to experiment with different distance metrics. It might make it easier for  $\hat{\mathcal{L}}$ to
learn $\bm{A}$, it might not\ldots}}

We select the "best" performing algorithm in $\hat{p}\id{z}$ 
by choosing the algorithm corresponding to the index of the min/max in $\hat{p}\id{z}$.
We'll refer to this algorithm as $\hat{\mathcal{A}}_*\id{z}$, the predicted best algorithm for instance $d\id{z}$.
\\ \texttt{\textit{Notating this is prickly due to the min/max nature of "best"\\\ldots Hopefully it should be clear.}}

We can measure $\hat{\mathcal{L}}$'s performance over a set of points $\mathcal{D}_\mathcal{F}$ in two ways.
In each case, the closer the measure is to $0$, the better $\hat{\mathcal{L}}$ is performing.
\begin{itemize}
    \item [Performance difference |]
        This is just the goal statement where our selected algorithm for $d\id{z}$ is $\hat{\mathcal{A}}_*\id{z}$.
        \[\color{black}
            \sum_{d\id{z} \in \mathcal{D}_\mathcal{F}}
            \abs{
                \hat{\mathcal{A}}\id{z}_*\left(d\id{z}\right)
                - \mathcal{A}\id{z}_*\left(d\id{z}\right)
            }
        .\]
        This is the less strict version of the two measurements as we may not be predicting $\hat{p}$ as close
        as possible to $p$ but we only care that the right algorithm was chosen.

    \item [Prediction difference |]
        As we are directly embedding into $\mathcal{P}$, we can use the distance of our prediction to
        the actual performance vector,
        \[\color{black}
            \sum_{d\id{z} \in \mathcal{D}_\mathcal{F}} \norm{\hat{p}\id{z} - p\id{z}}
            =
            \sum_{d\id{z} \in \mathcal{D}_\mathcal{F}} \norm{\hat{\mathcal{L}}\left(d\id{z}\right) - \bm{A}\left(d\id{z}\right)}
        .\]
        This is the more strict version of the two measurements as we are measuring $\hat{\mathcal{L}}$'s ability
        to predict the correct performance vector, not just that it selects the correct algorithm.
\end{itemize}

\newpage
\section{Siamese Architectures for Embedding}
A Siamese architecture seems to be often explained through the shared weights architecture, two
learners $\mathcal{L}_1$ and $\mathcal{L}_2$ sharing the same weights and trained using a combination of the distance between
their outputs and prior some prior knowledge about what these distances should be.
I find it more informative to think of it as a method of training a single learner $\hat{\mathcal{L}}$.
If this is the case then Siamese Architectures should really just be considered as a way to train
an embedding rather than a real architectural difference, illustrating the point that what really 
matters is not the architecture but where and what points you choose as well as how you choose their distances.

If this section is to be believed, I think talking generally about embeddings and training is more beneficial
than the use of the term Siamese Architecture.

I'll try show their equivalence now.

To do so I will show that $\mathcal{L}_1, \mathcal{L}_2$ and $\hat{\mathcal{L}}$ will produce the same output set
given the same input set. I will then show that data for training
$\mathcal{L}_1$ and $\mathcal{L}_2$ can equally be used to train a single $\hat{\mathcal{L}}$.
I will try to do this without introducing any symbol for weights and do so using only their
input set $\mathcal{D}_\mathcal{F}$ and their embedded output $\mathcal{D}_\mathcal{S}$.

\subsection{Asserting $\mathcal{L}_1\left(\mathcal{D}_\mathcal{F}\right) = \mathcal{L}_2\left(\mathcal{D}_\mathcal{F}\right)
= \hat{\mathcal{L}}\left(\mathcal{D}_\mathcal{F}\right)$}
Let's first assume we have a batch of data $\mathcal{D}_\mathcal{F}$. Passing this through the functions $\mathcal{L}_1$ and
$\mathcal{L}_2$ we get their respective embedding of the points, $\mathcal{D}_{\mathcal{S}_1}$ and $\mathcal{D}_{\mathcal{S}_2}$.

If the weights of $\mathcal{L}_1$ and $\mathcal{L}_2$ are tied then they will perform the same function
on any $d \in \mathcal{D}_\mathcal{F}$. As such we have,
\begin{align*}
    & \mathcal{L}_1\left(d\right) = \mathcal{L}_2\left(d\right)
    \\
    \implies & \mathcal{L}_1\left(\set{d\id{1}, \ldots, d\id{n}}\right) = \mathcal{L}_2\left(\set{d\id{1}, \ldots, d\id{n}}\right),
    \quad \forall d\id{z} \in \mathcal{D}_\mathcal{F}
    \\
    \implies & \mathcal{L}_1\left(\mathcal{D}_\mathcal{F}\right) = \mathcal{L}_2\left(\mathcal{D}_\mathcal{F}\right)
    \\
    \implies & \mathcal{D}_{\mathcal{S}_1} = \mathcal{D}_{\mathcal{S}_2},
\end{align*}
that is they both produce the same embedding for each datapoint, implying that we get the same
set out embedded points.
\texttt{\textit{This should be intuitive from the shared weights but this is just a more concrete argument.}}

As we have stated that $\hat{\mathcal{L}}$ has the same weights as both $\mathcal{L}_1$ and $\mathcal{L}_2$
then by substituting $\hat{\mathcal{L}}$ for either learner, we arrive at our claim
$\mathcal{L}_1\left(\mathcal{D}_\mathcal{F}\right)
 = \mathcal{L}_2\left(\mathcal{D}_\mathcal{F}\right)
 = \hat{\mathcal{L}}\left(\mathcal{D}_\mathcal{F}\right)$.

As each of them produce the same set of embedded points, we will simply refer to this embedded set of points 
as $\mathcal{D}_\mathcal{S}$.

\subsection{Training $\mathcal{L}_1$ and $\mathcal{L}_2$ can be made equivalent to training $\hat{\mathcal{L}}$}
Suppose that we have that $\mathcal{L}_1 = \mathcal{L}_2 = \hat{\mathcal{L}}$ by enforcing that they
each share the same weights.

If we can produce a training pair of embedded points
$\left(s_a,s_b\right) = \big(\mathcal{L}_1\left(d_a\right), \mathcal{L}_2\left(d_b\right)\big)$
using our Siamese Architecture $\mathcal{L}_1, \mathcal{L}_2$,
then we can also produce that same pair with our single learner, \textit{i.e.}
\[\color{black}
    \left(s_a,s_b\right)
    = \big(\mathcal{L}_1\left(d_a\right), \mathcal{L}_2\left(d_b\right)\big)
= \big(\hat{\mathcal{L}}\left(d_a\right), \hat{\mathcal{L}}\left(d_b\right)\big)
.\]
As any update to the weights are tied and all possible pairs $\left(s_a,s_b\right)$
can be produced by both the Siamese architecture $\mathcal{L}_1, \mathcal{L}_2$
and the single learner $\hat{\mathcal{L}}$, we have that any training
procedure for $\mathcal{L}_1, \mathcal{L}_2$ can be described for a single learner $\hat{\mathcal{L}}$.

\texttt{\textit{I haven't really proved training procedures are equivalent but
rather that $\hat{\mathcal{L}}$ can be trained to produce the same output and that
is has access to all the same \\information that can be used for training}}.





\texttt{\textit{Not sure if there is an assumption of a lower bound of 0, I'm guessing there is.}}


\end{document}
