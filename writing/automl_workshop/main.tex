\documentclass[twoside,11pt]{article}

% Any additional packages needed should be included after automl2019.
% Note that automl2019.sty includes epsfig, amssymb, natbib and graphicx,
% and defines many common macros, such as 'proof' and 'example'.
%
% It also sets the bibliographystyle to plainnat; for more information on
% natbib citation styles, see the natbib documentation, a copy of which
% is archived at http://www.jmlr.org/format/natbib.pdf
%
% This file is based on the JMLR.org formatting template
\usepackage{color,soul}
\usepackage{automl2020}

\usepackage{xcolor}
% Definitions of handy macros can go here

\newcommand{\dataset}{{\cal D}}
\newcommand{\fracpartial}[2]{\frac{\partial #1}{\partial  #2}}
\newcommand{\qquote}[1]{
    \begin{itemize}\item [] "{#1}" \end{itemize}
}
\newcommand{\lline}{\hfill\rule{\linewidth}{0.2pt}\hfill}

\newcounter{csuggestion}
\newcommand{\suggest}[1]{{\stepcounter{csuggestion}\color{purple!65} \textbf{S\thecsuggestion.} | \texttt{#1} }}
\newenvironment{suggestion}{
    \color{purple!65}
    \lline
    \textbf{S\thecsuggestion. |}% What an odd syntax to print the counter num
}
{
    \refstepcounter{csuggestion}
    \lline
}

% Heading arguments are {author-full-names}

\jmlrheading{A. Uthor and S. Cientist}

% Short headings should be running head and authors last names

\ShortHeadings{AutoML@ICML Per Instance Algorithm Selection}{Uthor and Cientist}
\firstpageno{1}

\begin{document}

\title{AutoML@ICML Per Instance Algorithm Selection}

\author{\name Bryan Tyrrell \email tyrrelbr@tcd.ie \\
       \addr Trinity College Dublin, Ireland
       \AND
       \name ??? Edward Bergman \email EDDIE EMAIL \\
       \addr Trinity College Dublin, Ireland
       \AND
       \name ??? Gareth Jones \email GARETH EMAIL \\
       \addr Dublin City University, Ireland
       \AND
       \name Joeran Beel \email joeran.beel@scss.tcd.ie \\
       \addr Trinity College Dublin, School of Computer Science and Statistics, Artificial Intelligence Discipline, ADAPT Research Centre, Ireland}

\maketitle

\begin{suggestion}
    Some global suggestions:
    \begin{itemize}
        \item Percentages could be reduced to single decimal in most places without losing
            contextual information.
        \item Some adjectives could be omitted or shortened without losing
            much. "best performing --- optimal"
    \end{itemize}
\end{suggestion}
\newpage

\begin{abstract}%   <- trailing '%' for backward compatibility of .sty file
    AutoML aims to increase the accessibility of machine learning by optimizing multiple facets of a machine learning pipeline.To date, the problem of algorithm selection is mainly viewed on a global level rather than on a per instance basis. \hl{It has been found in this paper that a 33.7315\% reduction in RSME could be achieved by a theoretically perfect per-instance selection algorithm over the best performing global solution.}

\begin{suggestion}
\texttt{Reducing the amount of extraneous adjectives and extra decimals
    makes it easier at a skim.}
\qquote{It has been found in this paper that a perfect
        per-instance selection of algorithms had $33.7\%$ reduced RSME when
        compared to that of selecting a single best algorithm.}

\end{suggestion}


This paper proposes a novel approach to algorithm selection on a per instance basis by leveraging the potential of Siamese neural networks to learn the similarity between the performance of dataset instances. \hl{By translating this \textbf{performance space to an embedding}, the Siamese neural network aims to predict the performance of future unseen data and hence, the most suitable algorithm on a per instance basis.}

\begin{suggestion}
\texttt{My understanding is that we are using a Siamese NN to translate
    \textit{\textbf{feature space to an embedding}}, allowing us to translate new
    data into this embedding space, using the performance space to
    train this Siamese NN. \\ \\
    I would also be cautious about using terms not defined or explained
    in the abstract. What's a performance space?
}

\end{suggestion}

Current results, run on a suite of 5 algorithms, show a 0.649\% reduction in RSME compared to the global best performing algorithm. Our approach produced the lowest RSME on a per instance basis 52.578\% of instances compared to 38.38\% from the global best performing algorithm. \suggest{One decimal places carries the same information here.}
\end{abstract}

\newpage
\section{Introduction}

The algorithm selection problem is the process of selecting, from a predefined pool of algorithms, the best algorithm to run for a given instance of an optimization problem \cite{RN17}. The main objectives are to \hl{reduce the time taken and the accuracy} \suggest{reduce accuracy, typo?} of the solution \cite{RN14}.

\begin{suggestion}
    \texttt{As citations like \cite{RN17} have no delimiter
    and look like normal text in a sentence, it might be
    better to use as part of a \\
    sentence rather than dropping it as a tag.}

    ".., the best algorithm to run for a given instance of an optimization problem, as presented in \cite{RN17}.
    The main objectives are to reduce the time taken and the accuracy of the solution provided by \cite{RN14}"

    \texttt{An alternative, if allowed, might be to use a different inline reference style.} 

\end{suggestion}


Automated Machine Learning (AutoML) is the process of selecting the correct algorithm, preprocessing steps and hyperparameters in an \hl{automatic streamlined} \suggest{automated} fashion to \hl{optimize and improve the} \suggest{automate a} pipeline \cite{RN26}. Multiple strategies have been proposed and implemented to automate the algorithm selection problem.  Libraries such as Auto-WEKA \cite{RN26} and Auto-Sklearn \cite{RN27} have automated the algorithm selection problem \hl{on a global level ie a dataset,} \suggest{"at a dataset level" OR " a global level"} with great success.

\hl{These solutions have focused on entire datasets (global level) and their goal is to select the globally best performing algorithm for the dataset.} \suggest{These solutions focused on selecting the globally best algorithm for the dataset.} In this paper, we focus on per instance algorithm selection which aims to select the best algorithm on a per instance basis achieving a much higher accuracy compared to its global alternative. On our dataset, a potential decrease of 33.7315\% \suggest{\ldots in RMSE} could be achieved by per instance selection algorithm \hl{compared} \suggest{when compared} to the global best performing algorithm. This is a pattern mirrored across multiple domains. Exploratory research conducted by \cite{RN4} in the field of micro level meta learners found that a 25.5\% reduction in root mean squared error (RSME) could be achieved by per instance selection algorithm tested using a suite of 8 algorithms.


The concept behind meta learning is “learning to learn” \cite{RN5} \suggest{"learning to learn", introduced by \cite{RN5}} which systematically observes how different machine learning approaches perform on a range of learning tasks, learning from this experience, and gaining meta-data to learn new tasks much faster and more accurately \cite{automl}. Meta learning was adopted into algorithm selection by \cite{michie1994machine} in 1994. To date, the field of Meta Learning has strongly focused on meta features to provide a solution to algorithm selection \suggest{, see} \cite{RN29} \cite{RN30} \cite{RN31}. This process creates new features based on the initial data to give  \hl{in-depth insight into } \suggest{a richer representation of} the data and provide increased accuracy of predictions when selecting the best performing algorithm. The above strategies focus on a single optimized algorithm.

While current solutions focus on creation of meta features based on the initial data, our solution focuses on learning the \hl{similarities and differences} \suggest{similarity} between algorithms in the performance space utilizing a Siamese neural network to learn these patterns. The label for our data is not inherently given, as such we propose a strategy to select and label \hl{these} instances \hl{using easy and difficult learning training instances} \suggest{according to a defined difficulty metric}.

\hl{The performance space to date has been seen as a product of algorithm selection} \suggest{I don't really understand this sentence}. We aim to leverage this space introducing to introduce a novel metric to express algorithm performance. This metric combines the error relative to the maximum possible error and the relative intra-datapoint performance (pairwise comparison with the best performing algorithm for each instance). This is done to expose and emphasize the performance patterns produced by individual algorithms on the data. To our knowledge the performance space has not been used in the building of a pipeline for algorithm selection.

\begin{suggestion}
    Some definition of what performance space is has to happen before
    reaching this point if we are to talk about it.

    Instead of talking about novel metric, it might make more sense to talk about
    this paragraph from the perspective of \textit{"normalizing"} or performances.

    \begin{itemize}
        \item [] "We normalize each performance to emphasis high performing algorithms,
            taking into account the size of possible errors and the performance of all
            other algorithms in the suite.
    \end{itemize}

\end{suggestion}

\section{Related Work}

Some of the current approaches to algorithm selection in the field of Meta-Learning include using data at a global, mid and micro level to create meta-learners. These terms are used to describe algorithms that process entire datasets, groupings of datasets and instances in a dataset respectively.


Global meta-learners have currently achieved good results. In the paper \cite{10.1145/1639714.1639734} a global meta-learner was implemented which created meta features based on user input and showed experimental results which outperformed other recommendations engines by a minimum of 6\% on 90,000 data instances.


A mid-level meta-learner was proposed by \cite{RN22} where they split a dataset into subsets in an attempt to select the best performing algorithm on the subset. They hoped that the strengths and weaknesses of individual algorithms could be exposed and exploited using the subsets. Their meta-learning approach achieved a RSME-0.78 which did not outperform the overall-best algorithm RSME-0.74  

 Micro level meta-learners attempt to predict performance on each instance of a dataset.  Exploratory research has been conducted into the field of micro level meta learners \cite{RN4}. They found that a 25.5\% RSME could be achieved by a theoretically perfect meta learner tested using a suite of 8 algorithms. Their final solution achieved an accuracy 2-3\% worse than the best global level meta learner SVD++. It performed 27\% worse than the theoretically perfect meta learner. As of yet, no per instance algorithm selection solution has outperformed global level meta-learning solutions.

Siamese Neural Networks have been implemented with high success rates in similarity classification problems such as signature verification \cite{RN2} and more recently one shot facial recognition \cite{RN19}. Siamese Neural Networks have been implemented for these problems due to the similarity of the data being inherently given such as two people or two signatures. For datasets where the data is a mix of categorical and numeric data such as ours, this similarity is not apparent to the human eye. 

Our aim in this paper is to transform instances features into embeddings through the Siamese neural network based on how similar the instance pair are in performance space. 



\section{Methodology}

Our meta learner predicts the best performing algorithm on a per instance basis chosen from a suite of algorithms. The algorithms used in this method include:
\begin{enumerate}
	\item Multi-layer Perceptron regression
	\item Lasso regression
	\item Random forest regression
	\item Stochastic Gradient Descent regression
	\item CatBoost regression
	\end{enumerate}
The Lending Club Loan dataset \cite{RN23} was used for the experiment. This dataset was chosen due to its large number of columns, 145, and rows, 2.26 million, which will increase the probability of the Siamese neural network finding similarity between instances. This assumption was made based on convolutional neural networks having had high success on images with large feature sets [12].  The label used from the dataset was the interest column which had a target range from 0 to 30.99\%.

The above suite of algorithms where run on a subset of 50,000 instances of the cleaned loan dataset. This subset was randomly split 80/20 training/test. The accuracy of each algorithm was measured using R-squared accuracy. Figure 1 below shows the accuracy of each algorithm.
 \begin{figure}[h]
\centering
\includegraphics[]{ PerformanceAlgorithmSuite.png}
\caption{Algorithm suite performance}
\end{figure}


As can be seen from figure 1 above, each algorithm performed to a very high standard. Once algorithm suite training completed, the trained models where used creation of the performance space.
 
The remaining cleaned dataset, 2.21 million instances, where randomly split 80/20 training/test and passed through each trained algorithm model in the suite to obtain a prediction for each instance. This value gave us our raw performance space. 

Equation

This raw performance space is then converted to the final performance using our novel metric shown in equation 1 above. This allows for .......... (explain why you convert the performance space)

This converted performance space above is used to create the labels needed for training in the Siamese neural network. Here 4 label types are proposed, easy positive training pairs, hard positive training pairs, easy negative training pairs and hard negative training pairs. An easy positive pair is one that is close in both feature space and performance space. This is a pair of instances that an observer would "expect" to perform similarly. A hard negative training pair is one are distant in the feature space yet perform  similar in the performance space. This thinking is also applied to negative training pairs.

What is classed as distant and similar is based on euclidean distance with selected margins. These margins are specific to each dataset and are decided based on sampling of average distance in each space. 

The Siamese network consists of 4 layers. The first and second consisting of 40 neurons, the third layer consisting of 30 and the output layer consisting of 20. All layers use the Relu activation function which the exception of the output, this was done to allow for easier clustering in later stages of this pipeline. The contrastive loss function, sometimes known as pairwise ranking loss, was used in this network. This loss function is a distance based loss function with its objective being to minimise distance d, between positive pairs and maximise the distance d between the negative pairs in the embedding space.



The final learned embeddings from the Siamese neural network are used in the classification of new training instances. All positive labeled data is sent through the learned network to harvest this learned embedding. The test data is also run through this network to receive a corresponding embedding. Using the training data as an embedding space a K nearest neighbour algorithm with 12 neighbours is run on each test embedding to find the 6 corresponding training instances.These 12 training instances are used to find the best performing algorithm which should be chosen for each test instance. The final algorithm is chosen by finding the best performing algorithm on the 12 training instances.



\section{Results}
An important part of this process is the conversion of the performance space using the novel metric proposed in equation X.X. This metrics goal is to XXXXXXXXXXXXXXX. A requirement of this metric is to 
preserve the rank of each performing algorithm as the goal of this pipeline is to predict the overall lowest rank from the algorithm suite. Write more about the metric plus how its conversion is helping from the gained data and not changing rank. 

Im not sure how to provide proof of how the metric helps other then showing no change in rank?

\subsection{Single-Algorithm, Meta-Learner and Oracle Performance}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{figures/singlealgorithms.png}
\caption{Single Learners Chart}
\end{figure}

\subsection{Frequency of Best-Performing Single Learners}

\begin{figure}[h]
\centering
\includegraphics[width=1.0 \columnwidth]{figures/rankdistribution.png}
\caption{Rank Distribution}
\end{figure}

\subsection{Performance Space}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{figures/performancespace.png}
\caption{Preformance Space}
\end{figure}

 \begin{figure}[h]
\centering
\includegraphics[]{AlgosResults.png}
\caption{Final Algorithm Performance on Test Data}
\end{figure}


Performance of each algorithm on the final 208,365 test instances are listed in the table above. The RSME of theoretically perfect meta learner, that is a learner that always picks the lowest absolute error or best performing algorithm is 0.444708. The single best algorithm from the suite was CatBoost with a RSME of 0.62515. 

Our system outperformed the best performing algorithm from the suite achieving a
RSME of 0.62098. Showing a reduction in RSME of 0.649\% from the global best performing algorithm, CatBoost and a reduction of 34.0285\% compared to the second best performing algorithm, MLP Regressor.

Another interesting gauge of performance is the frequency of correct selection of 
our algorithm. Our meta learner pick the best performing algorithm from the suite of algorithms 52.578\% of the time compared to CatBoost which was ranked as the 
best selection 38.38\%.

 \begin{figure}[h]
\centering
\includegraphics[]{SelectionFrequency.png}
\caption{Intra class prediction accuracy}
\end{figure}


Intra class prediction accuracy is also an important measure of the performance of the system. This measures the total amount of times each algorithm was selected and the number of times this selection was correct. In figure 3 above the intra class prediction accuracy can be seen. The learned network has a tendency to select CatBoost with double the selections compared to other classes. This could be due to the fact CatBoost it the best performing algorithm from the suite.



\section{Conclusion}
The paper has shown promising results with the current pipeline with a decrease in RSME compared to the globally best algorithm, CatBoost. There is still a large potential saving of RSME to a theoretically perfect meta learner. There are multiple avenues of further research to explore, the main areas of focus being the pairing system used to select the Siamese neural network, the novel metric proposed for modelling the performance space and the set up of the Siamese neural network.






% Acknowledgements should go at the end, before references and appendices

\acks{This research was partly conducted with the financial support of the ADAPT SFI Research Centre at Trinity College Dublin. The ADAPT SFI Centre for Digital Media Technology is funded by Science Foundation Ireland through the SFI Research Centres Programme and is co-funded under the European Regional Development Fund (ERDF) through Grant\# 13/RC/2106.}

\vskip 0.2in
\bibliography{sample}

% Appendix goes to a new page

\newpage


\end{document}
