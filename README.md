# Hello
This is a repo where I am keeping things related to my PhD that
are public. There are currently three folders, each containing their own readmes.
Any instructions provided will assume some Linux distribution.

* __Writing |__
    Surprising no one, this is where my TeX notes go. Pdfs for each document
    may or may not be included. If you would like to build the Pdfs yourself
    then please follow any instructions in the README contained in `writing`.

* __Code |__ 
    Yes, there is code in here, much wow. Code for generating plots,
    doing experiments or testing things out will be contained in here.
    Please see the README contained within for gettings things up and running.

* __Extra |__
    Its like a bag of pic-n-mix but sadly a lot less exciting. If there is something
    that doesn't fit in other folders or can't go in other folders (looking at you symlinks)
    then it will be in here. There may be a README in here, there may not...that's the beauty
    of a pic-n-mix, you never know what you're going to get.

