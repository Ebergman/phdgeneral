"""
A file for calculations of specific metrics on model predictions.
Note that this file is inefficient as it recalculates common pieces
between metrics and errors.

    python calc_metrics.py  \\
        --models 'data/my_predictions.csv' \\
        --dataset 'data/AugmentedTestLoanDataset.csv' \\
        --metrics 'RMSE'
"""
import numpy as np
import argparse
import json
import os.path as path
import pandas as pd

from loandata import LoanData, ErrorPredictions
from metrics import *
# ^ convenience but should be avoided

# Const stuff
info = {
    # Dict { model: 'path/to/predictions' }
    'models': {
        model: path.join('data', f'predictions_{model}_trained.csv')
        for model in [
            'MLPRegressor',
            'RandomForestRegressor'
        ]
    },
    # Dict { metric: metric_function }
    'metrics': {
        name: func
        for name, func in [
            ('rmse', rmse),
            ('regret', regret),
            ('selection_accuracy', selection_accuracy),
            ('overall_error', overall_error),
            ('mae', mae)
        ]
    },
    'algos': ['Lasso', 'SGD', 'CatBoost', 'MLP', 'Random Forest'],
    'dpath': path.join('data', 'AugmentedTestLoanDataset.csv')
}

# Small helpers
# Lambda argument captures with info=info, yes please...
# https://stackoverflow.com/questions/21053988/lambda-function-acessing-outside-variable
diff_header = lambda m, a: f'{m}_prediction_of_{a}_error'
model_path = lambda model, info=info: info['models'][model]

# argument parser constructing
parser = argparse.ArgumentParser(
    "Calculates metrics for the predictions of the baselines"
)
parser.add_argument('--metrics', '-m',
                    dest='metrics',
                    nargs='*',
                    choices=[metric for metric in info['metrics'].keys()],
                    default=[metric for metric in info['metrics'].keys()],
                    help='Metrics to evaluate')

parser.add_argument('--out', '-o',
                    action='store',
                    dest='out',
                    nargs='?',
                    metavar=('path'),
                    help='Set a csv file to write errors to. Will not \
                         calculate these if not set')

parser.add_argument('--models',
                    dest='models',
                    nargs='+',
                    choices=[model for model in info['models'].keys()],
                    default=[model for model in info['models'].keys()],
                    help='Models to evaluate predictions of')

parser.add_argument('--dataset', '-d',
                    action='store',
                    dest='dpath',
                    default=info['dpath'],
                    metavar=('path'),
                    help='Path to the Augmented Loan Dataset')

parser.add_argument('--verbose', '-v',
                    action='store_true')

def command_line_driver(args):
    """
    Implements a command line argument interface to
    metrics.py. This writes the result in txt file in JSON format.

    args | argparse.Namespace
        A list of arguments, see 'calc_metrics.py -h'
        for more help.
    """
    # Check all paths are val
    model_paths = [model_path(m) for m in args.models]
    for mpath in model_paths:
        assert path.isfile(mpath) , \
            f'{mpath} can not be read or does not exist.' \
            f'Did you mean to generate {mpath} first?'

    data_path = args.dpath
    assert path.isfile(data_path), \
        f'{data_path} can not be read or does not exist.' \
        f'You may need to download the data first.'

    if args.out:
        with open(args.out, 'w') as outfile:
                pass

    # Load dataset
    dataset = LoanData(data_path, standardize_features=False)
    errs = dataset.errors()

    # Calculate per instance things to file if --out specified
    if args.out:
        df = pd.DataFrame()
        for model in args.models:

            preds = ErrorPredictions(path=model_path(model)).values()

            if args.verbose:
                print(f'Calculating {model} prediction_differences')

            headers_diff = (diff_header(model, a) for a in info['algos'])
            differences = np.asarray(prediction_differences(preds, errs))
            for header, column in zip(headers_diff, differences.T):
                df[header] = column

            if args.verbose:
                print(f'Calculating {model} prediction_distances')

            h_dist = f'{model}_prediction_distance_to_error'
            distances = np.asarray(prediction_distances(preds, errs))
            df[h_dist] = distances

            if args.verbose:
                print(f'Calculating {model} mean error between each algo prediction and algo error')

            header_avg_pred_err = f'{model}_mean_prediction_error'
            avg_pred_errs = np.asarray(mean_prediction_errors(preds, errs))
            df[header_avg_pred_err] = avg_pred_errs

            if args.verbose:
                print(f'Calculating {model} selection indices')

            header_selections = f'{model}_selected_algorithm'
            model_selections = selections(preds)
            df[header_selections] = model_selections

            if args.verbose:
                print(f'Calculating {model} selection_difference')

            header_selection_diff = f'{model}_selection_difference'
            selection_diffs = selection_differences(preds, errs)
            df[header_selection_diff] = selection_diffs

        if args.verbose:
            print(f'Writing measures to {args.out}')
        df.to_csv(args.out, index=False)

    # Calculate metrics over the entire prediction set
    results = { metric : {} for metric in info['metrics'].keys() }

    for model, fpath  in info['models'].items():
        if args.verbose:
            print(f'Evaluating {model} at {fpath}')

        preds = ErrorPredictions(path=fpath).values()

        # Metrics
        for metric, func in info['metrics'].items():

            if metric in args.metrics:
                result = func(preds, errs)
                results[metric][model] = result

                if args.verbose:
                    print(f'{model} {metric} is {result}')

    results_string = json.dumps(results, sort_keys=True, indent=4)
    print(results_string)

if __name__ == "__main__":
    args = parser.parse_args()
    command_line_driver(args)
