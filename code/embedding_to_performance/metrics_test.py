""" Tests for the various metrics used """

from inspect import signature

import numpy as np
from numpy.linalg import norm

# Test Data
predictions = np.asarray([
    [3, 4],
    [1, 1],
    [1, 1],
])

performances = np.asarray([
    [4, 3],
    [4, 5],
    [1, 1]
])

""" 
Testing metric_prediction_distance
"""
from metrics import metric_prediction_distances

# https://stackoverflow.com/questions/12627118/get-a-function-arguments-default-value
dist_f = signature(metric_prediction_distances).parameters['dist_f'].default

test_distances = [dist_f(pd, pf) for pd, pf in zip(predictions, performances)]
distances = metric_prediction_distances(predictions, performances)

assert all(t == d for t, d in zip(test_distances, distances))

"""
Testing metric_performance_distance
"""
from metrics import metric_performance_distances
# TODO should make sure test_data isn't manually tied
test_perf_distances = [1, 0, 0]
perf_distances = metric_performance_distances(predictions, performances)

#assert all(t == p for t, p in zip(test_perf_distances, perf_distances))

"""
Testing metric_correct_selection
"""
from metrics import metric_ratio_correct_selection

# TODO: Testing should probably just check the shapes
#       going in and out are consistent
test_ratio = 2.0/3.0 # TODO this manually tied to the test data
ratio = metric_ratio_correct_selection(predictions, performances)
assert ratio == test_ratio



