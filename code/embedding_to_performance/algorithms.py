"""
This files handles a command line interface to list, train and
evaluate supported algorithms.

# TODO (I havn't tested this since messing with dataset.py, can't garuantee it
#       will work)
To include an algorithm it must have the methods,

    __init__(**params) # params are defined in algorithms.json

    fit(x, y) -> None
    predict(x') -> y'

...where x, x' are features and y, y' are labels

Example:
```
import catboost:

def CatboostWrapper:

    def __init__(self, params):
        self.algorithm = CatBoostRegressor(**params)

    def fit(self, x, y):
        self.algorithm.fit(x, y) # Catboost convienently has the same interface

    def predict(self, x):
        return self.algorithm.predict(x) # Same as above

supported_algorithms['catboost'] = CatboostWrapper
```

There needs to be a corresponding entry in 'algorithms.json', see
that file for more information.
"""
import argparse
import json
import numpy
import pandas
import pickle
import sklearn

from os.path import join

from list_info import list_info

info_path = 'algorithms.json'
outdir_models = join('data', 'models')
outdir_predictions = join('data', 'predictions')

supported_algorithms = {
    'lasso': sklearn.linear_model.Lasso,
    'sgd': sklearn.linear_model.SGDRegressor,
    'mlp': sklearn.neural_network.MLPRegressor,
    'randomforest': sklearn.ensemble.RandomForestRegressor,
    'adaboost': sklearn.ensemble.AdaBoostRegressor,
    'bagging': sklearn.ensemble.BaggingRegressor,
    'extratrees': sklearn.ensemble.ExtraTreesRegressor,
    'gradientboosting': sklearn.ensemble.GradientBoostingRegressor,
    'huberregressor': sklearn.linear_model.HuberRegressor,
    'passiveagressive': sklearn.linear_model.PassiveAggressiveRegressor,
    'ransac': sklearn.linear_model.RANSACRegressor,
    'theilsenregressor': sklearn.linear_model.TheilSenRegressor,
    'decisiontree': sklearn.tree.DecisionTreeRegressor,
    'extratree': sklearn.tree.ExtraTreeRegressor,
}

train_dataset = "algorithm_training"
test_dataset = "algorithm_testing"

parser = argparse.ArgumentParser(f"""
    =====
    Usage
    =====

    --list {trained, untrained, evaluated} [model], [predictions], [url]

        *   Filter by 'trained', 'untrained', 'evaluated'.
            Include paths to 'model', 'predictions'.
            Include 'url' to model documentation.

    --train {untrained, untrained, all, [algo1, algo2, ...]}

        *   Use '--train untrained' to train untrained algorithms.
            Use '--train all' to train all algorithms.

            $ python {__file__} --train lasso sgd
            > Trained
                sgd, data/models/sgd.pkl
                lasso, data/models/lasso.pkl


    --eval {all, [algo1, algo2, ...]}

        *   Use '--eval all' to evaluate all trained algorithms.

            Example:
                $ python {__file__} --train lasso sgd
                > Evaluated
                    sgd, data/evals/sgd.pkl
                    lasso, data/evals/sgd.pkl
    """)

parser.add_argument('--list',
                    '-l',
                    nargs='*',
                    default=None)

parser.add_argument('--eval',
                    '-e',
                    nargs='+',
                    default=None)

parser.add_argument('--train',
                    '-t',
                    nargs='+',
                    default=None)
def get_info():
    """ Helper to return the info dict from file """
    info = {}
    with open(info_path, 'r') as f:
        info = json.load(f)

    return info

def save_info(info):
    """ Helper to save info dict to file """
    with open(info_path, 'w') as f:
        json.dump(info, f, indent=4)

def list_handler(list_args):
    """ Handles listing out algorithm information """
    info = get_info()
    params = { item: True for item in list_args }

    items = list_info(info, **params)

    print('\n'.join(items))

def train_handler(train_args):
    """ Handles training algorithms """
    info = get_info()

    # Get the selected algorithm names
    selected = []
    if train_args == ['all']:
        selected = info.keys()

    elif train_args == ['untrained']:
        selected = [
            name for name, algoinfo in info.items()
            if algoinfo['model'] is None
        ]

    else:
        assert all(name in info.keys() for name in train_args), \
            f'{name} was not contained {info.keys()}'
        selected = train_args

    # Load in dataset
    data = datasets.get(train_dataset)

    # Train each algorithm
    for name in selected:
        print(f'Training {name}')
        params = info[name]['params']

        algo = train(name, params, data.x(), data.y())

        constructor = supported_algorithms[name]
        model = constructor(**params)
        model.fit(data.x(), data.y())

        path = join(outdir_models, f'{name}_model.pkl')
        with open(path, 'wb') as f:
            pickle.dump(algo, f)

        print(f'Saved to {path}')
        info[name]['model'] = path

        save_info(info)

def eval_handler(eval_args):
    """
    Evaluates algorithms on the dataset and saves
    their results as a single colummn csv
    """
    info = get_info()

    # Select algorithms and verify they're trained
    selected = []
    if eval_args == ['all']:
        selected = [
            (name, algo_info['model'])
            for name, algo_info in info.items()
            if algo_info['model'] is not None
        ]
    else:
        for name in eval_args:
            model_path = info[name]['model']
            assert model_path is not None, \
                f'{name} not trained'
            selected.append((name, model_path))

    if selected == []:
        print('No algorithms were trained')

    # Check the paths exists
    for name, path in selected:
        assert os.path.exists(path), \
            f'Model for {name} was not found at \n\t{path}'

    # Load dataset
    data = datasets.get(dataset_test)

    # Evaluate each algorithm
    for name, path in selected:
        print(f'Evaluating {name}')

        model = None
        with open(path, 'rb') as f:
            model = pickle.load(f)

        y = model.predict(data.x())

        filename = f'{name}_predictions.csv'

        outpath = join(outdir_predictions, filename)
        print(f'Predictions at {outpath}')

        head = f'{name}_label_predictions'
        numpy.savetxt(outpath,
                      y,
                      delimiter=', ',
                      header=head,
                      comments='')

        info[name]['predictions'] = outpath
        save_info(info)

if __name__ == "__main__":
    args = parser.parse_args()
    # TODO not checking if multiple options specified

    if args.list is not None:
        list_handler(args.list)
    elif args.eval is not None:
        eval_handler(args.eval)
    elif args.train is not None:
        train_handler(args.train)
    else:
        print('Invalid usage, see --help')
