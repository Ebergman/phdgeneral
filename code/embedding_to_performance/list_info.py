def list_info(info,
              url=False,
              model=False,
              params=False,
              trained=False,
              untrained=False,
              evaluated=False,
              predictions=False):
    """
    Filters information from an info store and returns
    the list of item info as strings.

    Params
    ======
    info | Dict
        Contains the algorithms information, see 'info.json'

    url | bool : False
        Include the url in the list

    model | bool : False
        Include the model filepath in the list

    params | bool : False
        Include the params in the list

    trained | bool : False
        Include only trained algorithms

    untrained | bool : False
        Include only untrained algorithms

    evaluated | bool : False
        Include only algoritmhs that have been evaluated

    predictions | bool : False
        Include the model predictions path in the list

    Returns
    =======
    List[str]
        Returns a list of information about each algorithm
    """
    assert not (trained and untrained), \
        'Can not specify trained and untrained'
    assert not (untrained and evaluated), \
        'Can not specify untrained and evaluated'

    if trained:
        info = {k:v for k, v in info.items() if v['model'] is not None}
    if untrained:
        info = {k:v for k, v in info.items() if v['model'] is None}
    if evaluated:
        info = {
            k:v for k, v in info.items() if v['predictions'] is not None
        }

    # Specifies key to a bool of whether it should be included
    include = {
        'url': url,
        'model': model,
        'params': params,
        'predictions': predictions
    }

    items = []
    for name, algo_info in info.items():
        entry = [name]
        entry += [
            str(algo_info[key])
            for key, should_include in include.items()
            if should_include is True
        ]
        string = ', '.join(entry)
        items.append(string)

    return items
