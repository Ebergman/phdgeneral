def evaluate(algorithm, x):
    """
    Evaluates the algorithm on the dataset.
    Returns its predictions as a pandas series

    Params
    ======
    algorithm | has 'predict(x: np.ndarray) -> np.ndarray'
        The algorithm to evaluate, must support a
        'predict(x) -> y' function that takes an
        ndarray and returns an ndarray of predictions.

    dataset | np.ndarray
        The features to predict on

    Returns
    =======
    np.ndarray
        The predictions of the algorithm on the features
    """
    return algorithm.predict(x)
