"""
A file for getting datasets.

Datasets are described in the Dicts <datasets> with
the columns list in <columns>.

Provides an easy 'get()' function to get the dataset needed.
"""
import json
import pandas
import argparse

from os.path import join, exists

from sklearn.preprocessing import LabelEncoder

import metrics

algorithm_info_path = 'algorithms.json'
metalearner_info_path = 'metalearners.json'
algoinfo = json.load(open(info_path, 'r'))
algorithm_names = algoinfo.keys()

default_dir = join('data', 'datasets')

columns = {
    'label' : 'int_rate',
    'features_all' : [
        'id', 'member_id', 'loan_amnt', 'funded_amnt', 'funded_amnt_inv', 'term',
        'installment', 'grade', 'sub_grade', 'emp_title', 'emp_length',
        'home_ownership', 'annual_inc', 'verification_status', 'issue_d',
        'loan_status', 'pymnt_plan', 'url', 'desc', 'purpose', 'title', 'zip_code',
        'addr_state', 'dti', 'delinq_2yrs', 'earliest_cr_line', 'inq_last_6mths',
        'mths_since_last_delinq', 'mths_since_last_record', 'open_acc', 'pub_rec',
        'revol_bal', 'revol_util', 'total_acc', 'initial_list_status', 'out_prncp',
        'out_prncp_inv', 'total_pymnt', 'total_pymnt_inv', 'total_rec_prncp',
        'total_rec_int', 'total_rec_late_fee', 'recoveries',
        'collection_recovery_fee', 'last_pymnt_d', 'last_pymnt_amnt',
        'next_pymnt_d', 'last_credit_pull_d', 'collections_12_mths_ex_med',
        'mths_since_last_major_derog', 'policy_code', 'application_type',
        'annual_inc_joint', 'dti_joint', 'verification_status_joint',
        'acc_now_delinq', 'tot_coll_amt', 'tot_cur_bal', 'open_acc_6m',
        'open_act_il', 'open_il_12m', 'open_il_24m', 'mths_since_rcnt_il',
        'total_bal_il', 'il_util', 'open_rv_12m', 'open_rv_24m', 'max_bal_bc',
        'all_util', 'total_rev_hi_lim', 'inq_fi', 'total_cu_tl', 'inq_last_12m',
        'acc_open_past_24mths', 'avg_cur_bal', 'bc_open_to_buy', 'bc_util',
        'chargeoff_within_12_mths', 'delinq_amnt', 'mo_sin_old_il_acct',
        'mo_sin_old_rev_tl_op', 'mo_sin_rcnt_rev_tl_op', 'mo_sin_rcnt_tl',
        'mort_acc', 'mths_since_recent_bc', 'mths_since_recent_bc_dlq',
        'mths_since_recent_inq', 'mths_since_recent_revol_delinq',
        'num_accts_ever_120_pd', 'num_actv_bc_tl', 'num_actv_rev_tl',
        'num_bc_sats', 'num_bc_tl', 'num_il_tl', 'num_op_rev_tl', 'num_rev_accts',
        'num_rev_tl_bal_gt_0', 'num_sats', 'num_tl_120dpd_2m', 'num_tl_30dpd',
        'num_tl_90g_dpd_24m', 'num_tl_op_past_12m', 'pct_tl_nvr_dlq',
        'percent_bc_gt_75', 'pub_rec_bankruptcies', 'tax_liens', 'tot_hi_cred_lim',
        'total_bal_ex_mort', 'total_bc_limit', 'total_il_high_credit_limit',
        'revol_bal_joint', 'sec_app_earliest_cr_line', 'sec_app_inq_last_6mths',
        'sec_app_mort_acc', 'sec_app_open_acc', 'sec_app_revol_util',
        'sec_app_open_act_il', 'sec_app_num_rev_accts',
        'sec_app_chargeoff_within_12_mths', 'sec_app_collections_12_mths_ex_med',
        'sec_app_mths_since_last_major_derog', 'hardship_flag', 'hardship_type',
        'hardship_reason', 'hardship_status', 'deferral_term', 'hardship_amount',
        'hardship_start_date', 'hardship_end_date', 'payment_plan_start_date',
        'hardship_length', 'hardship_dpd', 'hardship_loan_status',
        'orig_projected_additional_accrued_interest',
        'hardship_payoff_balance_amount', 'hardship_last_payment_amount',
        'disbursement_method', 'debt_settlement_flag', 'debt_settlement_flag_date',
        'settlement_status', 'settlement_date', 'settlement_amount',
        'settlement_percentage', 'settlement_term'
    ],
    'features' : [
        'id', 'member_id', 'loan_amnt', 'funded_amnt', 'term',
        'installment', 'grade', 'sub_grade', 'emp_length',
        'home_ownership', 'annual_inc', 'verification_status', 'issue_d',
        'loan_status', 'pymnt_plan', 'desc', 'purpose', 'title', 'zip_code',
        'addr_state', 'dti', 'delinq_2yrs', 'earliest_cr_line', 'inq_last_6mths',
        'mths_since_last_delinq', 'mths_since_last_record', 'open_acc', 'pub_rec',
        'revol_bal', 'revol_util', 'total_acc', 'initial_list_status',
        'total_pymnt', 'total_pymnt_inv', 'total_rec_prncp',
        'total_rec_int', 'total_rec_late_fee', 'recoveries',
        'last_pymnt_d', 'last_pymnt_amnt',
        'next_pymnt_d', 'last_credit_pull_d', 'collections_12_mths_ex_med',
        'mths_since_last_major_derog', 'policy_code', 'application_type',
        'annual_inc_joint', 'dti_joint', 'verification_status_joint',
        'acc_now_delinq', 'tot_coll_amt', 'tot_cur_bal', 'open_acc_6m',
        'open_act_il', 'open_il_12m', 'open_il_24m', 'mths_since_rcnt_il',
        'total_bal_il', 'il_util', 'open_rv_12m', 'open_rv_24m', 'max_bal_bc',
        'all_util', 'total_rev_hi_lim', 'inq_fi', 'total_cu_tl', 'inq_last_12m',
        'acc_open_past_24mths', 'avg_cur_bal', 'bc_open_to_buy', 'bc_util',
        'chargeoff_within_12_mths', 'delinq_amnt', 'mo_sin_old_il_acct',
        'mo_sin_old_rev_tl_op', 'mo_sin_rcnt_rev_tl_op', 'mo_sin_rcnt_tl',
        'mort_acc', 'mths_since_recent_bc', 'mths_since_recent_bc_dlq',
        'mths_since_recent_inq', 'mths_since_recent_revol_delinq',
        'num_accts_ever_120_pd', 'num_actv_bc_tl', 'num_actv_rev_tl',
        'num_bc_sats', 'num_bc_tl', 'num_il_tl', 'num_op_rev_tl', 'num_rev_accts',
        'num_tl_120dpd_2m', 'num_tl_30dpd',
        'num_tl_90g_dpd_24m', 'num_tl_op_past_12m', 'pct_tl_nvr_dlq',
        'percent_bc_gt_75', 'pub_rec_bankruptcies', 'tax_liens', 'tot_hi_cred_lim',
        'total_bal_ex_mort', 'total_bc_limit', 'total_il_high_credit_limit',
        'revol_bal_joint', 'sec_app_earliest_cr_line', 'sec_app_inq_last_6mths',
        'sec_app_mort_acc', 'sec_app_open_acc', 'sec_app_revol_util',
        'sec_app_open_act_il', 'sec_app_num_rev_accts',
        'sec_app_chargeoff_within_12_mths', 'sec_app_collections_12_mths_ex_med',
        'sec_app_mths_since_last_major_derog', 'hardship_flag', 'hardship_type',
        'hardship_reason', 'hardship_status', 'deferral_term', 'hardship_amount',
        'hardship_start_date', 'hardship_end_date', 'payment_plan_start_date',
        'hardship_length', 'hardship_dpd', 'hardship_loan_status',
        'orig_projected_additional_accrued_interest',
        'hardship_payoff_balance_amount', 'hardship_last_payment_amount',
        'disbursement_method', 'debt_settlement_flag', 'debt_settlement_flag_date',
        'settlement_status', 'settlement_date', 'settlement_amount',
        'settlement_percentage', 'settlement_term'
    ],
    'drop_columns' : [
        'collection_recovery_fee', 'funded_amnt_inv', 'num_rev_tl_bal_gt_0',
        'num_sats', 'out_prncp', 'out_prncp_inv', 'url', 'emp_title'
    ],
    'label_encode_columns' : [
        'term', 'grade', 'sub_grade', 'home_ownership', 'verification_status',
        'issue_d','loan_status', 'pymnt_plan', 'purpose', 'title', 'zip_code',
        'addr_state', 'earliest_cr_line', 'initial_list_status', 'last_pymnt_d',
        'last_credit_pull_d', 'application_type', 'hardship_flag',
        'disbursement_method', 'debt_settlement_flag'
    ],

    'algorithm_predictions' : [
        f'{name}_predicitons' for name in algorithm_names
    ],
    'algorithm_errors' : [
        f'{name}_errors' for name in algorithm_names
    ],
    'algorithm_ranks' : [
        f'{name}_ranks' for name in algorithm_names
    ],

    'metalearner_prediction_distance' : 'metalearner_prediction_distances',

    'metalearner_predictions' : [
        f'metalearner_predictions_for_{name}'
        for name in algorithm_names
    ],
    'metalearner_errors': [
        f'metalearner_errors_for_{name}'
        for name in algorithm_names
    ],
    'metalearner_ranks': [
        f'metalearner_ranks_for_{name}'
        for name in algorithm_names
    ]
}

datasets = {
    'unprocessed' : {
        'x': columns['features'],
        'y': columns['label'],
        'path': join(default_dir, 'loan.csv')
    },
    'algorithm_full': {
        'x': columns['features'],
        'y': columns['label'],
        'path': join(default_dir, 'loan_processed.csv')
    },
    'algorithm_training' : {
        'x': columns['features'],
        'y': columns['label'],
        'path': join(default_dir, 'algorithm_training.csv')
    },
    'algorithm_testing' : {
        'x': columns['features'],
        'y': columns['label'],
        'path': join(default_dir, 'algorithm_testing.csv')
    },
    'metalearner_full' : {
        'x': columns['features'],
        'y': columns['algorithm_errors'],
        'path': join(default_dir, 'metalearner_full.csv')
    },
    'metalearner_training' : {
        'x': columns['features'],
        'y': columns['algorithm_errors'],
        'path': join(default_dir, 'metalearner_training.csv')
    },
    'metalearner_testing' : {
        'x': columns['features'],
        'y': columns['algorithm_errors'],
        'path': join(default_dir, 'metalearner_testing.csv')
    }
}

def get_info(infoname):
    """ Helper to return the info dict from file """
    info = {}
    with open(info_path, 'r') as f:
        info = json.load(f)

    return info

def save_info(info):
    """ Helper to save info dict to file """
    with open(info_path, 'w') as f:
        json.dump(info, f, indent=4)

def process(fpath=datasets['unprocessed']['path'],
            out=datasets['algorithm_full']['path'],
            verbose=False):
    """
    Process the kaggle loan dataset

    Expects a path to the loan kaggle dataset found at:
    https://www.kaggle.com/wendykan/lending-club-loan-data/data
    """
    assert exists(fpath), \
        f'Could not find {fpath}, has it been downloaded?'

    missing_limit = 0.05

    if verbose: print('-- Reading csv')

    df = pandas.read_csv(fpath)

    if verbose: print(f'-- Dropping columns')

    # TODO reason behnd dropping these
    for column_name in columns['drop_columns']:

        if verbose: print(f'\t{column_name}')
        df.drop(column_name, axis=1, inplace=True)

    if verbose: print(f'-- Dropping columns with {missing_limit} missing entries')

    for column_name in list(df.columns):

        n_valid = df[column_name].count()
        missing_percent = df[column_name].isnull().mean()

        if missing_percent > missing_limit:

            if verbose: print(f'\t{column_name} : {missing_percent:.2f}')
            df.drop(column_name, axis=1, inplace=True)

    if verbose: print(f'-- Dropping rows with missing entries')

    df.dropna()

    if verbose: print(f'-- Encoding columns')

    for column_name in columns['label_encode_columns']:

        if verbose: print(f'\t{column_name}')
        values = df[column_name].unique()
        encoder = LabelEncoder()
        df[column_name] = encoder.fit_transform(df[column_name].astype(str))

    # TODO seems like a bad idea to normalize on min max
    # https://stackoverflow.com/a/41532180
    # Seems uncertain as to the use of this snippet normalizes
    # as a matrix or column for column
    # ```
    #   df= (df-dataframe.min())/(dataframe.max()-dataframe.min())
    # ```
    # Might be nice to make it an implicit column-wise operation
    if verbose: print(f'-- Normalize columns')
    for column_name in df.columns:

        if verbose: print(f'\t{column_name}')
        x = df[column_name]
        df[column_name] = (x - x.min()) / (x.max() - x.min())

    # Finally, write it out
    print(f'-- Writing to {out}, Warning: This may take a long while as its writing a large csv')
    df.to_csv(out, chunksize=100000)
    print(f'Processed {fpath}\nWritten to {out}')


def split(fpath, out_left, out_right, left_proportion):
    """ Splits a dataset into two """
    print(f'-- Reading {fpath}')
    full = pandas.read_csv(fpath)

    n_split = int(left_proportion * len(full))

    print(f'-- Writing {n_split} samples to {out_left}')
    left = full.iloc[0:n_split, :]
    left.to_csv(out_left)

    print(f'-- Writing {len(full) - n_split} samples to {out_right}')
    right = full.iloc[n_split:-1, :]
    right.to_csv(out_right)


def create_metalearner_full_dataset(outpath=datasets['metalearner_full']['path']):
    """
    Creates the datasets for the metalearners based from all evaluated 
    algorithms.
    """
    algo_info = get_info(algorithms_info_path)

    evaluated_algorithms = {
        name: info['predictions']
        for name, info in algo_info.items()
        if info['predicitons'] is not None
    }
    evaluated_names = evaluated_algorithms.keys()
    evaluated_paths = evaluated_algorithms.values()

    ds_tested_on = get('algorithm_testing') # Has features, label

    # Get the predictions of the algorithms
    predicitons = [ pandas.read_csv(path) for path in evaluated_paths]

    # Calculate and add errors for each column of predicitons
    label_column = columns['label']
    labels = df[label_column]


    # Create pandas dataframe of errors
    errors = pandas.DataFrame()
    for name, predictions in zip(evaluated_names, evaluated_predicitons):
        algo_errors = metric.algorithm_label_error(labels, predictions)
        colummn_name = f'{name}_errors'
        errors[column_name] = algo_errors

    # Calculate and add ranks
    ranks = []
    for name,  in 

    column_name = f'{name}_errors'

    pass

def create_results_dataset(outpath=datasets['metalearner_test_results']['path']):
    """
    Creates the results for metalearner predictions
    """
    pass


class DatasetWrapper:
    """ Provides an x, y interface to the dataset """

    def __init__(self, dataframe, description):
        self.df = dataframe
        self.x_headers = description['x']
        self.y_headers = description['y']

    def x():
        return self.iloc[:, self.x_headers].to_numpy()

    def y():
        return self.iloc[:, self.y_headers].to_numpy()


def get(name):
    """ Returns a little wrapper around the dataset {name} """
    assert name in datasets.keys(), f'{name} is not a know dataset'

    description = datasets[name]
    path = description.path

    if name == 'unprocessed':
        assert exists(path), f"""
            Please download csv data from from
                https://www.kaggle.com/wendykan/lending-club-loan-data/data
        """
    elif name == 'algorithm_full':
        assert exists(path), f"""
            Processed data was not found at {path}.

            Preprocess data with

                $ python {__file__} \\
                        --preprocess "{path}" \\
                        --out "{datasets['unprocessed']['path']}"

        """
    elif name == 'algorithm_training' or name == 'algorithm_testing':
        assert exists(path), f"""
            Data for algorithms not found at {path}.

            Process the raw csv first

            $ python {__file__} --process {datasets['unprocessed']['path']}

            and split with

                $ python {__file__} \\
                    --split "{datasets['algorithm_full']['path']}"
                    --left "{datasets['algorithm_training']['path']}"
                    --right "{datasets['algorithm_test']['path']}"

        """
    elif name == 'metalearner_training' or name == 'metalearner_testing':
        assert exists(path), f"""
            Data for algorithms not found at {path}.

            Process the raw csv first

            $ python {__file__} --process {datasets['unprocessed']['path']}

            and split with

                $ python {__file__} \\
                    --split "{datasets['algorithm_full']['path']}"
                    --left "{datasets['algorithm_training']['path']}"
                    --right "{datasets['algorithm_test']['path']}"

            Train the algorithms with

                $ python algorithms.py \\
                    --train untrained

            and evaluate them with

                $ python algorithms.py
                    --eval all
                    --out "{datasets['metalearner_full']['path']}"

            splitting again with

                $ python {__file__} \\
                    --split "{datasets['metaleaner_full']['path']}"
                    --left "{datasets['metalearner_training']['path']}"
                    --right "{datasets['metalearner_test']['path']}"
        """
    else:
        print('Unrecognized file in dataset.keys, {path}')

    dataframe = pandas.read_csv(path)
    return DatasetWrapper(dataframe)


# Create the parser to use this the split and process functions
parser = argparse.ArgumentParser(f"""
    =====
    Usage
    =====

    --process [--verbose]

        * Process the dataset that should be located at
            {datasets['unprocessed']['path']}


    --split {{ algorithm_dataset, metalearner_dataset }} [--verbose]

        * Splits either the <algorithm_dataset> located at
            {datasets['algorithm_full']['path']}

        ..or the <metalearner_dataset> located at
            {datasets['algorithm_full']['path']}

    --create_metalearner_full_dataset [--verbose]

        *   Creates the metalearner data from algorithm predictions

    """)

parser.add_argument('--verbose', action='store_true')
parser.add_argument('--process', action='store_true')
parser.add_argument('--create_metalearner_full_dataset', action='store_true')
parser.add_argument('--split',
                    choices=['algorithm_dataset', 'metalearner_dataset'])

if __name__ == '__main__':
    args = parser.parse_args()

    if args.process:
        process(verbose=args.verbose)

    if args.create_metalearner_full_dataset:
        create_metalearner_full_dataset(verbose=args.verbose)

    elif args.split == 'algorithm_dataset':
        ds = datasets['algorithm_full']['path']
        left = datasets['algorithm_training']['path']
        right = datasets['algorithm_testing']['path']

        # Using 5% of data to train as algorithms converge quite quick,
        # allowing us to use more of the data to train the meta learners
        split(ds, left, right, left_proportion=0.05)

    elif args.split == 'metalearner_dataset':
        ds = datasets['metalearner_full']['path']
        left = datasets['metalearner_training']['path']
        right = datasets['metalearner_testing']['path']

        # We let the meta learners train on a lot more of the instances
        split(ds, left, right, left_proportion=0.9)

    else:
        f"""Invalid usage

            $ python {__file__} --help

        """
