"""
A file for measuring the differences in model predictions
and actual performances.

It is also not optmized for np arrays.
"""
import numpy as np
from numpy.linalg import norm

default_distance_func = lambda a, b: norm(a - b)

def algorithm_label_error(labels, predictions):
    """
    Returns the difference between the label and the predictions of an algorithm

    Params
    ======
    labels | ndarray (n_instances,)
        The lables to predict

    predictions | ndarray (n_instances,)
        The predictions of a single algorithm

    Returns
    =======
    ndarray (n_instances,)
        The difference between them
    """
    return np.abs(labels - predictions)

def algo_ranks(errors):
    """
    Returns the row wise rank of each algorithm error

    Params
    ======
    errors | ndarray (n_instances, n_algorithms)
        The errors of the algorithms

    Returns
    =======
    ndarray (n_instances, n_algorithms)
        The row-wise rank of each algorithm based on its prediction error
    """
    # +1 is to make rankings start from one
    return np.argsort(errors) + 1


def prediction_distances(predictions,
                            performances,
                            distance=default_distance_func):
    """
    Compute the distance between each pair of points,
    'pred' and 'perf'.

    Params
    ======
    predictions | List[vec]
        The predicted performances of a suite of algorithms.
        (Should match shape of <performances>.)

    performances | List[vec]
        The actual performances of a suite of algorithms.

    dist_f | Callable [vec, vec -> float] : np.linalg.norm
        A function giving the distance between two points.

    Returns
    =======
    List
        A List of distances between each prediction and the actual
        performances.
    """
    return [
        distance(pred, perf)
        for pred, perf in zip(predictions, performances)
    ]

def prediction_differences(predictions, performances, best=None):
    """
    Returns the element wise difference between predictions and
    performances, rather than `prediction_distances` which gives
    the vector distance from prediction to performance.

    Params
    ======
    predictions | List[vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | List[vec float]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | any : None
        Not used, acts as placeholder to keep function interface.

    Returns
    =======
    List[vec float]
        A list where the vector is (pred_vec - perf_vec) between
        predictions and performances.
    """
    return [
        np.asarray(pred) - np.asarray(perf)
        for pred, perf in zip(predictions, performances)
    ]

def selection_differences(predictions, performances, best=np.argmin):
    """
    Computes the difference in performances between the selection of
    algorithms and the best algorithms.

    Params
    ======
    predictions | List[vec]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | List[vec]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | Callable [vec -> int] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    List[float]
        A List of difference in performance between the chosen
        algorithm and the best algorithm for each instance.
    """
    selected = map(best, predictions)
    selected_performances = [
        perf[i] for i, perf in zip(selected, performances)
    ]

    correct = map(best, performances)
    correct_performances = [
        perf[i] for i, perf in zip(correct, performances)
    ]

    return list(
        np.abs(s - c)
        for s, c in zip(selected_performances, correct_performances)
    )

def mean_prediction_errors(predictions, performances, best=np.argmin):
    """
    Returns the list of:
        The mean difference between an algorithm performances
        and their predicted performance.

    Params
    ======
    predictions | list [vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | list [vec float]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | Callable [ vec -> int ] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    List[float]
        Returns the mean error in prediction for an algorithms
        performance, for each instace.
    """
    differences = (
        np.asarray(pred) - np.asarray(perf) # vec - vec = vec
        for pred, perf in zip(predictions, performances)
    )
    return [np.mean(np.abs(dif)) for dif in differences]

def average_error_in_selection(predictions, performances, best=np.argmin):
    """
    Returns the averages of the error in prediction to performance
    for each algorithm.

    Params
    ======
    predictions | list [vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | list [vec float]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | Callable [ vec -> int ] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    float
        Returns the mean error from a sequence of algorithms selected
        by the predicitons.
    """
    selected = map(best, predictions)
    selected_performances = [
        perf[i] for i, perf in zip(selected, performances)
    ]
    return np.mean(np.abs(selected_performances))

def selections(predictions, performances=None, best=np.argmin):
    """
    Returns the index of the algorithm selected.

    Params
    ======
    predictions | list [vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | any : None
        Not used, kept for interface.

    best | Callable [ vec -> int ] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    List[int]
        Returns the index of the algorithm selected based on predictions
    """
    return list(map(best, predictions))

def selection_accuracy(predictions, performances, best=np.argmin):
    """
    Computes the ratio of how many times
    the best algorith was chosen by a model.

    Params
    ======
    predictions | list [vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | list [vec float]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | Callable [ vec -> int ] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    float in [0,1]
        The ratio of how often the chosen algorithm was
        the best algorithm
    """
    selected = map(best, predictions)
    correct = map(best, performances)
    equal = list(s == c for s, c in zip(selected, correct))
    return sum(equal) / float(len(equal))

def regret(predictions, performances, best=np.argmin):
    """
    Calculates the regret between the selected algorithms
    and the best selection of algorithms.

    Regret is:
        sum(perf(selected_algos)) - sum(perf(best_algos))

    Params
    ======
    predictions | list [vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | list [vec float]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | Callable [vec -> int] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    float
        The difference in performance between the selection
        of algorithms and the best selection.

    Regret is:
        sum(perf(selected_algos)) - sum(perf(best_algos))
    """
    selected = map(best, predictions)
    selected_performances = [
        perf[i] for i, perf in zip(selected, performances)
    ]

    correct = map(best, performances)
    correct_performances = [
        perf[i] for i, perf in zip(correct, performances)
    ]

    return sum(
        np.abs(s - c)
        for s,c in zip(selected_performances, correct_performances)
    )

def rmse(predictions, performances, best=np.argmin):
    """
    Calculates the overall RMSE of the predictions.
    This function assume that we are talking about error
    due to the 'E' in RMSE. Hence by default we take the
    best algorithm to be the one with the least estimated error.

    Params
    ======
    predictions | list [vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | list [vec float]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | Callable [vec -> int] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    float
        The sq. root of the mean of the sum of (errors)^2.
        (The size of the average error vector for a set of vectors)
    """
    selected = map(best, predictions)
    selected_performances = [
        perf[i] for i, perf in zip(selected, performances)
    ]
    return np.sqrt(np.mean(np.power(selected_performances, 2)))

def overall_performance(predictions, performances, best=np.argmin):
    """
    Calculates the overall performance of the selected algorithms.

    Params
    ======
    predictions | list [vec float]
        The predicted performances of a suite of algorithms.
        (Should match shape of label_performances.)

    performances | list [vec float]
        The performances of a suite of algorithms.
        (Should match shape of model_predictions.)

    best | Callable [ vec -> int ] : np.argmin
        A function returning the index of the <best> algorithm
        in each prediction.

    Returns
    =======
    float
        The sum of all the errors by the selected algorithms
    """
    selected = map(best, predictions)
    selected_performances = [
        perf[i] for i, perf in zip(selected, performances)
    ]
    return sum(selected_performances)

# Aliases
from functools import partial
mae = partial(average_error_in_selection, best=np.argmin)
overall_error = partial(overall_performance, best=np.argmin)
