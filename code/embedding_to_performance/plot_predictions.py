"""
Plots errors and predictions.
"""
import os.path as path
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
#sns.set()

# I should look at this module
from matplotlib import collections as mc

from loandata import LoanData, ErrorPredictions

n_points = 10
show = False # Saves if True

algos = ['Lasso', 'SGD', 'CatBoost', 'MLP', 'Random Forest']
algo1 = 'Lasso'
algo2 = 'SGD'
assert algo1 in algos, f'\n-a1 {algos}\n\t{algo1} was passed'
assert algo2 in algos, f'\n-a2 {algos}\n\t{algo2} was passed'

model = 'MLPRegressor'
models = ['RandomForestRegressor', 'MLPRegressor']
assert model in models, f'\n-m {models}\n\t{model} was passed'

kinds = ['trained', 'untrained']
kind = 'trained'
assert kind in kinds, f'\n-k {kinds}\n\t"{kind}" was passed'

paths = {
    'test_data' : path.join('data', 'AugmentedTestLoanDataset.csv'),
    'predictions' : path.join('data',
                              f'predictions_{model}_{kind}.csv'),
    'outpath' : path.join('data',
                          f'{model}_{kind}_{n_points}points.png')
}


# Get data
predict_path = paths['predictions']
predictions = ErrorPredictions(predict_path, nrows=n_points)

dataset_path = paths['test_data']
dataset = LoanData(dataset_path, nrows=n_points)
errors = dataset.errors()

# Create figure
fig, ax = plt.subplots()

# Get errors for two chosen algorithms idx_algo1, idx_algo2
i_algo1 = algos.index(algo1)
i_algo2 = algos.index(algo2)

# Turns the errors of algo1 and algo2 into `npoint` points
errors_algo1 = errors[:, i_algo1]
errors_algo2 = errors[:, i_algo2]


# Turn predictions in `npoint` points
preds_algo1 = predictions[:, i_algo1]
preds_algo2 = predictions[:, i_algo2]

# Line segments consisting of [ (err_point, pred_point) , ... ]
pred_points = zip(preds_algo1, preds_algo2)
err_points = zip(errors_algo1, errors_algo2)
line_segments = list(zip(err_points, pred_points))

lines_look = {
    'colors' : list('0.75' for i in range(0, len(line_segments))),
    'linewidths' : 1
}
lc = mc.LineCollection(line_segments, **lines_look)
ax.add_collection(lc)

# Draw things 
ax.scatter(errors_algo1, errors_algo2, c='black', marker='o', linewidth=2, label='actual')
ax.scatter(preds_algo1, preds_algo2, c='black', marker='.', linewidth=0.5, label='predictions')

# Cosmetic touches
ax.set_title(paths['predictions'] + ', ' + str(n_points) + ' points')
ax.set_xlabel(algo1 + ' error')
ax.set_ylabel(algo2 + ' error')
ax.legend()
ax.autoscale()
ax.margins(0.1)

if show:
    plt.show()
else:
    fig.savefig(paths['outpath'])
    s = f"Saving plot of {kind} {model} to {paths['outpath']}"
    print(s)
