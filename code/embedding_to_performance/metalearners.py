import pandas
from os.path import join, exists

from sklearn.neural_network import MLPRegressor

import datasets

info_path = 'baselines.json'
outdir_models = join('data', 'models')
outdir_predictions = join('data', 'predictions')
training_dataset = 'metalearner_training'
testing_dataset = 'metalearner_testing'

supported_metalearners = {
    'mlpregressor' : MLPRegressor
}

def get_info(fpath=info_path):
    """ Helper to return the info dict from file """
    info = {}
    with open(info_path, 'r') as f:
        info = json.load(f)

    return info


def save_info(info):
    """ Helper to save info dict to file """
    with open(info_path, 'w') as f:
        json.dump(info, f, indent=4)


def train(name, data, outdir=outdir_models):
    """
    Trains the metalearner on the dataset and writes the predicitons
    to

        {outdir}/metalearner_{name}_model.pkl
    """
    assert name in supported_metalearners.keys(), \
        f'{name} not a supported metalearner'

    info = get_info()
    params = info[name]['params']
    constructor = supported_metalearners[name]

    print('-- Fitting {name}')
    metalearner = constructor(**params)
    metalearner.fit(data.x(), data.y())

    outpath = join(outdir, f'metalearner_{name}_model.pkl')

    print('-- Saving {name} model to {outpath}')
    with open(outpath, 'wb') as f:
        pickle.dump(metalearner, f)

    info[name]['model'] = outpath
    save_info(outpath)


def eval(name, data, outdir=outdir_predictions):
    """
    Evaluates the metalearner on a dataset and
    writes the results to outdir with the name

        {outdir}/metalearner_{name}_predictions.csv
    """
    assert name in supported_metalearners.keys(), \
        f'{name} not a supported metalearner'

    info = get_info()
    metalearner_path = info[name]['model']

    print(f'-- Loading model for {name} from {metalearner_path}')
    metalearner = None
    with open(metalearner_path, 'rb') as f:
        metalearner = pickle.load(f)

    print(f'-- Evaluating metalearner {name}')
    y = metalearner.predict(data.x())

    prediction_headers = [
        f'{name}_prediction_of_{algo_header}' for algo_header in data.x_headers
    ]
    df = pandas.DataFrame(
        { header: x for (header, x) in zip(prediciton_headers, y.T)}
    )

    outpath = join(outdir, f'metalearner_{name}_predictions.csv')
    print(f'-- Writing results to {outpath}')
    df.to_csv(outpath)


parser = argparse.ArgumentParser(f"""
    =====
    Usage
    =====

    --train

        *   Trains the baselines to predict algorithm errors
            from features.

    --eval

        *   Evaluates the baseline on a test set
    """)

parser.add_argument('--train', action='store_true')
parser.add_argument('--eval', action='store_true')
parser.add_argument('--verbose', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()

    # TODO only implemented mlpregressor for now as a metalearner
    if args.train:
        name = 'mlpregressor'
        data = datasets.get(training_dataset)
        train(name, data)

    elif args.test:
        name = 'mlpregressor'
        data = datasets.get(training_dataset)
        train(name, data)
