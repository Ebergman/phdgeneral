# Python Environment
The code section of this repo is run under a python virtual environment
that should manage all dependancies.

To use this repo I recommend `pipenv`.

Navigate to code folder.
```BASH
$ cd code
```

Install dependancies from the Pipfile.
```BASH
$ pipenv install
```

Enter virtual environment.
```BASH
$ pipenv shell
```

You should now be able to run all python code.
