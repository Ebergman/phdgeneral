"""
Handles reading from data from datastores
Small items should be stored in a .json
Larger tabular items should be stored in a .csv
"""
import os
import json

# https://stackoverflow.com/questions/26646362/numpy-array-is-not-json-serializable
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def load(filename):
    """
    Load in data from a datastore.

    Params
    ======
    filename
        The file to read in from.
        Handles: json, csv
    """
    ext = os.path.splitext(filename)[-1].lower()

    if ext == ".json": 
        f = open(filename, "r")
        data = json.load(f)
        f.close()
        return data

    elif ext == ".csv":
        print(f"Is .csv file")

    else:
        print(f"Can't handle file {filename}")

def save(data, filename):
    """
    Save data into a datastore

    Params
    ======
    data
        The data to store

    filename
        The filename to save to
    """
    ext = os.path.splitext(filename)[-1].lower()

    if ext == ".json":
        f = open(filename, "w")
        json.dump(data, f, indent=2, cls=NumpyEncoder)
        f.close()

    elif ext == ".csv":
        print("Implement me")

    else:
        print(f"Do not know how to handle file {filename} of type {ext}")

