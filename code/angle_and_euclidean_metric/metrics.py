import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

from store import store

datastore = './store/datastore.json'
data = store.load(datastore)

image_save_as = 'combined_metric2.png'

def create_custom_metric(rp, alpha=0.2):
    """
    Return a function that will calculate the distance from some reference
    point over a numpy array
    """
    def func(p, q):
        x = np.asarray([p, q])
        cos = np.dot(x, rp) / (np.linalg.norm(x) * np.linalg.norm(rp))
        dist = np.linalg.norm(x - rp)
        #return dist
        #return np.power(1 - cos, alpha)
        return np.power(1 - cos, alpha) * np.power(dist, 0.5)

    return np.vectorize(func)

def plot_custom_metric(
        data,
        save_as="Custom_Metric_Colour_Contour.png", 
        contour_lines=30,
        step=0.1,
    ):
    """
    Plots a contour map of custom metric, saves figure with <save_as>
    If save_as = "", it does NOT save

    data should have "performances", "alphas"
    """
    fig, axes = plt.subplots(2, 2, figsize=(10,10))
    axes = axes.reshape(-1)

    points = np.arange(0, data['bound'], step)
    X, Y = np.meshgrid(points, points)

    rp = np.asarray(data['reference_point'])
    ps = np.asarray(data['performances'])
    print([p for p in ps])
    


    for (ax, alpha) in zip(axes, data['alphas']):
        custom_metric = create_custom_metric(rp, alpha)
        Z = custom_metric(X, Y)
        contours = ax.contourf(X, Y, Z, contour_lines, cmap='viridis_r')
        ax.scatter(ps[:,0], ps[:,1], c='b')
        ax.scatter(rp[0], rp[1], c='r')
        ax.set_ylabel(r'performance $\mathcal{A}_2$')
        ax.set_xlabel(r'performance $\mathcal{A}_1$')

        for p in ps:
            dist = custom_metric(p[0], p[1])
            dist_rounded = round(np.asscalar(dist), 4)
            ax.annotate(dist_rounded, p, p + [0.15, 0.15])

        ax.set_title(r'$\alpha = ' + str(alpha) + '$')

    fig.suptitle(r'$(1-cos(\theta))^\alpha new one$')
    #fig.suptitle(r'$(1-cos(\theta))^\alpha$')
    #fig.suptitle(r'$\Vert a - b \Vert$')
    #fig.tight_layout(pad=1)

    if save_as != "": 
        plt.savefig(save_as, dpi=300)

    plt.show()

plot_custom_metric(data, save_as=image_save_as)

